实验1：
====
环境：
- 配置了react-redux和redux-thunk
- Import 
- mapstatetoprops

变数：
  在componentDidMount里调用一个action：
    - this.props.动作()  //顺利被redux-thunk检测到，2次dispatch进行异步操作。
    - 动作() //被正常执行，action一脸懵逼，一次dispatch都没有被执行。
结论：一定要用经过connect()以参数方式传递的action。

实验2：
====
环境：
- 配置了react-redux和redux-thunk
- Import 
- mapstatetoprops

变数：
  在componentDidMount里调用n个this.proprs.动作
    - n=1 //正常执行
    - n=2 //正常执行
结论：非常正常，需要注意的是每次action触发，所有reducer都会被匹配一次，匹配不到会执行default，所以default通常要return oldState.

实验3
===
react和redux执行顺序
结论：reducer先被扫3遍，内部初始化一下，
        -结果是所有default被调用了三次，不一定按顺序。
     然后root component开始执行
        -connect()
        -render()
        -componentdidmount
          -触发componentdidmount()里面的action
            -actions是并发的，然后reducers被触发
        -每次reducer被改变都会使用重新执行root component一次
        -每一次有新的props，componentdidupdate()都会被执行
实验4
==
在顶层componentdidmount已经有loadUsers()
在wokerlist里面在componentdidmount使用 判断触发this.props.loadUsers(),会导致其它store里面的异步数据异常。但是在render使用就没有问题。如果顶层componentdidmount没有loadusers,workerlist的loadusers可以完美运作。
结论：不要同时在顶层和子组件同时触发同一个actoin？

实验5
==
在顶层componentdidmount有actoin触发，
在子组件有action在render或componentdidmount触发都会造成顶层action触发失败。但是由onclick造成的却不会。