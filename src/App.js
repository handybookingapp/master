import './styles/App.scss';

import React from 'react';
import { withRouter, Route, Switch } from 'react-router-dom';
import { SignUp2, SignUp, Login, Logout, NavBar, PostTask } from './common';
import { HomePage, WorkerList, TaskList, PrivateUserProfile, UserprofilePublic } from './pages';

import faker from 'faker';
import './styles/App.scss';

import { connect } from 'react-redux';
import { loadUsers, loadUser, logIn, logOut, signUp } from './redux/user/actions';
import { loadTasks, loadTasksByFilter, loadTasksByUserID } from './redux/task/actions';

// import Task from './pages/workerList/Task';
import { getCurrentID } from './Util';


class App extends React.Component {

  updateLoginStatus() {
    let userID = getCurrentID();
    let currentUser = this.props.currentUser;
    // //console.log(userID);
    if (userID !== null) {
      if (currentUser === null) {
        try {
          this.props.logIn(getCurrentID());
        } catch (e) {
          if (e.message === 'Request failed with status code 401') {
            this.props.logOut();
          }
        }
      } else if (currentUser === "") {//currentUser === "", no user info for such user
        let currentPath = document.location.pathname;
        // console.log(currentPath, (/\/(sign-up-step2)$/.test("" + currentPath)));
        if (!(/\/(sign-up-step2)$/.test(currentPath))) {
          document.location.href = currentPath === "/" ? "/sign-up-step2" : currentPath + "/sign-up-step2";
        }
      }
    } else if (currentUser !== null) {
      this.props.logOut();
    }
  }

  componentDidMount() {
    // let storage = window.localStorage;
    // storage.removeItem('token');

    this.updateLoginStatus();

  }
  componentDidUpdate() {
    this.updateLoginStatus();
  }
  render() {
    // console.log('render', this.props.currentUser, this.props.tasks);

    //如果有id 但是没有info的话，要返回首页并且显示signup2
    const NotFound = () =>
      <div>
        <h3>404 page not found</h3>
        <p>We are sorry but the page you are looking for does not exist.</p>
      </div>

    let currentPath = this.props.location.pathname;
    // console.log(this.props.users)

    return (

      <main className="app">

        {/* <Redirect to="/sign-up-step2" /> */}



        <NavBar
          logIn={this.props.logIn}
          logOut={this.props.logOut}
          loadUsers={this.props.loadUsers}
          loadTasks={this.props.loadTasks}
          currentPath={currentPath}
          currentUser={this.props.currentUser} />

        {/* same as using exact */}
        <Switch>


          <Route path='/tasks/id/:id'
            component={() =>
              <TaskList
                tasks={this.props.tasks}
                loadTasks={this.props.loadTasks}
                loadTasksByFilter={this.props.loadTasksByFilter}
                loadTasksByUserID={this.props.loadTasksByUserID}
                currentUser={this.props.currentUser}
              />}
          />
          <Route path='/tasks/id/:id/post-similar-task'
            component={() =>
              <TaskList
                tasks={this.props.tasks}
                loadTasks={this.props.loadTasks}
                loadTasksByFilter={this.props.loadTasksByFilter}
                loadTasksByUserID={this.props.loadTasksByUserID}
                currentUser={this.props.currentUser}
              />}
          />
          <Route path='/tasks/id/:id/edit-task'
            component={() =>
              <TaskList
                tasks={this.props.tasks}
                loadTasks={this.props.loadTasks}
                loadTasksByFilter={this.props.loadTasksByFilter}
                loadTasksByUserID={this.props.loadTasksByUserID}
                currentUser={this.props.currentUser}
              />}
          />

          <Route path='/tasks/user/:user'
            component={() =>
              <TaskList
                tasks={this.props.tasks}
                loadTasks={this.props.loadTasks}
                loadTasksByFilter={this.props.loadTasksByFilter}
                loadTasksByUserID={this.props.loadTasksByUserID}
                currentUser={this.props.currentUser}
              />}
          />

          <Route path='/tasks(/category/:category)?(/title/:title)?'
            component={() =>
              <TaskList
                tasks={this.props.tasks}
                loadTasks={this.props.loadTasks}
                loadTasksByFilter={this.props.loadTasksByFilter}
                loadTasksByUserID={this.props.loadTasksByUserID}
                currentUser={this.props.currentUser}
              />}
          />
          {/* TODO: if there is no id provided, redirect  to another page*/}
          {/* TODO: need to change,should implement API, should pass userid then fetch user */}
          <Route path='/profile/:id' component={() => (<UserprofilePublic loadUser={this.props.loadUser} user={this.props.user} />)} />

          <Route path='/workers/:cat' component={() => (<WorkerList users={this.props.users} tasks={this.props.task_worklist} loadUsers={this.props.loadUsers} currentPath={currentPath} />)} />
          <Route path='/workers/' component={() => (<WorkerList users={this.props.users} tasks={this.props.task_worklist} loadUsers={this.props.loadUsers} currentPath={currentPath} />)} />

          <Route path='/privateuserprofile' component={PrivateUserProfile} />

          <Route path='/'
            component={() =>
              <HomePage
                tasks={this.props.tasks}
                users={this.props.users}
                reviews={this.props.reviews}
                loadUsers={this.props.loadUsers}
                loadTasks={this.props.loadTasks}

              />}
          />
          <Route path='/a' component={NotFound} />
        </Switch>

        <Route path='/*/post-task' component={() => <PostTask currentUser={this.props.currentUser} />} />
        <Route path='/post-task' component={() => <PostTask currentUser={this.props.currentUser} />} />


        <Route path='/*/sign-up' component={SignUp} />
        <Route path='/sign-up' component={SignUp} />

        <Route path='/*/login' component={() => <Login logIn={this.props.logIn} currentPath={currentPath} />} />
        <Route path='/login' component={() => <Login logIn={this.props.logIn} currentPath={currentPath} />} />

        <Route path='/*/sign-up-step2' component={() => <SignUp2 logOut={this.props.logOut} />} />
        <Route path='/sign-up-step2' component={() => <SignUp2 logOut={this.props.logOut} />} />

        <Route path='/*/logout' component={Logout} />
        <Route path='/logout' component={Logout} />


        <footer>
          <p>Handy Hero</p>
          <p>Designed and Developed by Coders</p>
          <p> All rights reserved © 2019 </p>
        </footer>

      </main>
    );
  }
}
function mapStateToProps(state) {
  //console.log("mapStaToProps:", state);
  return {
    // tasks: makeTasks(20),
    tasks: state.tasks,
    // task_worklist: makeTasks(9),
    currentUser: state.currentUser,
    users: state.users,
    user: state.user,
    reviews: makeReviews(20)
  }
}
export default connect(mapStateToProps, { loadTasksByFilter, loadTasksByUserID, loadTasks, loadUsers, loadUser, logIn, logOut, signUp })(withRouter(App));

//TODO: not sure if we will implement this bit.
export function makeReviews(n) {
  let reviews = [];
  for (let i = 0; i < n; i++) {
    let review =
    {
      avatar: faker.image.avatar(),
      review: faker.lorem.sentences(1),
      bookkeep: faker.lorem.sentences(1),
      money: faker.random.number(100)
    }

    reviews.push(review);
  }
  return reviews;
}



