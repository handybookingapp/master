import axios from "axios";
import { createBrowserHistory } from 'history';
import jwt_decode from 'jwt-decode'
// import mock from './services/mockAxios';

// import backendAPI from './services/backendAPI';

axios.interceptors.request.use(config => {
	if (localStorage.token) {
		config.headers.Authorization = 'Bearer ' + localStorage.token;
	}
	return config;
}, function (error) {
	return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
	if (response.data.errno === 401) {
		createBrowserHistory.push('/login');
		//console.log("token expires");
	}
	return response;
}, function (error) {
	return Promise.reject(error);
});



/**
 * @description
 * post data with jwt.
 */
async function postData(url, param) {
	return await axios.post(url, param);
}

async function putData(url, param) {
	return await axios.put(url, param);
}

async function deleteData(url) {
	return await axios.delete(url);
}

async function getData(url, param) {
	return await axios.get(url, {
		params: param
	});
}

function getCurrentUser() {
	let token = window.localStorage.getItem('token');
	if (token) {
		let payload = jwt_decode(token);
		return payload.email;
	}
	else
		return null;
}

function getCurrentID() {
	let token = window.localStorage.getItem('token');
	if (token) {
		let payload = jwt_decode(token);
		// console.log(payload._id);
		return payload._id;
	}
	else
		return null;
}

export { postData, getData, getCurrentUser, getCurrentID, putData, deleteData };
