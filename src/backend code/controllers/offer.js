const { errorHandler } = require('../helpers');
const status = require('http-status');

const Offer = require('../models/offer');

const offerController = {

  index: async (req, res) => {
    try {
      //console.log('index: ' + req.params);
      const items = await offerController.find({}).exec();
      res.send(items);
    } catch (e) {
      //console.log('catch: ' + e.name);
      errorHandler(e, res);
    }
  },

  create: async (req, res) => {
    try {
      //console.log('req body: ' + req.body);
      const item = new Offer(req.body);
      await item.save();
      res.status(status.CREATED).send(item);
    } catch (e) {
      errorHandler(e, res);
      //console.log(e.name);
    }
  },

  update: async (req, res) => {
    const { id } = req.params;
    const item = await Offer.findByIdAndUpdate(id, req.body, { new: true });
    res.status(status.OK).send(item);
  },

  delete: async (req, res) => {
    const { id } = req.params;
    await Task.findByIdAndRemove(id);
    res.sendStatus(status.NO_CONTENT);
  },

  show: async (req, res) => {
    const { id } = req.params;
    const item = await Offer.findById(id).exec();
    res.status(status.OK).send(item);
  },

};

module.exports = offerController;