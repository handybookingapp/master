const { errorHandler } = require('../helpers');
const status = require('http-status');

const Task = require('../models/task');

const taskController = {

	index: async (req, res) => {
		try {
			const items = await Task.find({}).populate('userinfo').exec();
			res.send(items);
		} catch (e) {
			//console.log('catch: ' + e.name);
		}
	},

	create: async (req, res) => {
		try {
			const item = new Task(req.body);
			await item.save();
			res.status(status.CREATED).send(item);
		} catch (e) {
			errorHandler(e, res);
		}
	},

	update: async (req, res) => {
		const { id } = req.params;
		try {
			const item = await Task.findByIdAndUpdate(id, req.body, { new: true });
			res.status(status.OK).send(item);
		} catch (e) {
			errorHandler(e, res);
		}
	},

	delete: async (req, res) => {
		const { id } = req.params;
		console.log("\n\n\n", id, "\n\n\n");

		try {
			await Task.findByIdAndRemove(id);
			res.sendStatus(status.NO_CONTENT);
		} catch (e) {
			errorHandler(e, res);
		}
	},

	show: async (req, res) => {
		const { id } = req.params;
		const item = await Task.findById(id).exec();
		res.status(status.OK).send(item);
	},

	showByTitle: async (req, res) => {
		try {
			const { url } = req.params;
			const item = await Task.showByTitle(url);
			if (!item) {
				res.sendStatus(status.NO_CONTENT);
			}
			else {
				res.status(status.OK).send(item);
			}
		} catch (e) {
			errorHandler(e, res);
		}

	},

	findByUserID: async (req, res) => {
		try {
			const { user } = req.params;
			const item = await Task.findByUserID(user);
			if (!item) {
				res.sendStatus(status.NO_CONTENT);
			}
			else {
				res.status(status.OK).send(item);
			}
		} catch (e) {
			errorHandler(e, res);
		}
	},

	//request fired from a search action tasklist page.
	findByCategoryAndTitle: async (req, res) => {
		try {
			//console.log("in controller function findByCategoryAndTitle")
			const { category, title } = req.params;

			const item = await Task.findByCategoryAndTitle(category, title);
			if (!item) {
				res.sendStatus(status.NO_CONTENT);
			}
			else {
				res.status(status.OK).send(item);
			}
		} catch (e) {
			errorHandler(e, res);
		}
	},

};

module.exports = taskController;