const { errorHandler } = require('../helpers');
const status = require('http-status');

const User = require('../models/user_basic');
const userController = {

	index: async (req, res) => {
		const users = await User.find({})
			.select({
				//'password': 0,
			}).exec();
		res.send(users);
	},

	create: async (req, res) => {
		try {
			//console.log('req: ' + req.body);
			const newUser = new User(req.body);
			await newUser.save();
			//res.status(status.CREATED).send(newUser);
			res.status(status.CREATED).send({ token: newUser.generateJwt() });
		} catch (e) {
			//res.status(200).send(e);
			errorHandler(e, res);
			//console.log(e.name);
		}
	},

	update: async (req, res) => {
		const { id } = req.params;
		const user = await User.findByIdAndUpdate(id, req.body, { new: true });
		res.status(status.OK).send(user);
	},

	delete: async (req, res) => {
		const { id } = req.params;
		await User.findByIdAndRemove(id);
		res.sendStatus(status.NO_CONTENT);
	},

	show: async (req, res) => {
		//console.log('show...');
		const { id } = req.params;
		const user = await User.findById(id).exec();
		res.status(status.OK).send(user);
	},

	login: async (req, res) => {
		const { email, password } = req.body;
		//console.log('%O', req.body);
		const user = await User.findByEmailPassword(email, password);
		if (!user) {
			res.sendStatus(status.UNAUTHORIZED);
		}
		else {
			res.status(status.OK).send({ token: user.generateJwt() });
		}
		//console.log(res);
	}
};

module.exports = userController;