require('dotenv').config();
const status = require('http-status');
const path = require('path');
const fs = require('fs');

const { errorHandler } = require('../helpers');
const UserInfo = require('../models/user_info');

const userInfoController = {

  index: async (req, res) => {
    try {
      //console.log('index: ' + req.params);
      const userinfos = await UserInfo.find({}).exec();
      res.send(userinfos);
    } catch (e) {
      //console.log('catch: ' + e.name);
      errorHandler(e, res);
    }
  },

  create: async (req, res) => {
    try {
      //console.log('req body: ' + req.body);
      const userInfo = new UserInfo(req.body);
      await userInfo.save();
      res.status(status.CREATED).send(userInfo);
    } catch (e) {
      console.log(e.name);

      errorHandler(e, res);
    }
  },

  // update: async (req, res) => {
  //   const { id } = req.params;
  //   const userinfo = await UserInfo.findByIdAndUpdate(id, req.body, { new: true });
  //   res.status(status.OK).send(userinfo);
  // },

  updateByBasicID: async (req, res) => {
    const { basicId } = req.params;
    const userinfo = await UserInfo.findOneAndUpdate({ user: basicId }, req.body, { new: true });
    res.status(status.OK).send(userinfo);
  },

  // delete: async (req, res) => {
  //   const { basicId } = req.params;
  //   await UserInfo.findByIdAndRemove(basicId);
  //   res.sendStatus(status.NO_CONTENT);
  // },
  deleteByBasicID: async (req, res) => {
    const { basicId } = req.params;
    await UserInfo.findOneAndRemove({ user: basicId });
    res.sendStatus(status.NO_CONTENT);
  },

  showByBasicID: async (req, res) => {
    const { basicId } = req.params;
    const userinfo = await UserInfo.findOne({ user: basicId }).exec();
    res.status(status.OK).send(userinfo);
  },

  uploadAvatar: async (req, res) => {

    // const newFilename = 'upload/' + req.body.user + "_avatar";
    // //console.log(newFilename);
    // await fs.rename(req.file.path, newFilename, function (err) {
    // 	if (err) {
    // 		throw err;
    // 	}
    // 	console.log('upload done');
    // })

    const { user } = req.body;
    //delete old avatar
    let userinfo = await UserInfo.findOne({ user: user }).exec();
    console.log(userinfo.avatar);
    await deleteFile(userinfo.avatar);

    //update new file URL
    req.body.avatar = req.file.path;
    console.log(JSON.stringify(req.file) + JSON.stringify(req.body));
    userinfo = await UserInfo.findOneAndUpdate({ user: user }, req.body, { new: true });
    res.status(status.OK).send(userinfo.avatar);

    // res.writeHead(status.OK, { "Access-Control-Allow-Origin": "*" });
    // res.write(userinfo.avatar);
    // res.end();
  },

  findByCategory: async (req, res) => {
    try {
      const { category } = req.params;
      const item = await UserInfo.findByCategory(category);
      if (!item) {
        res.sendStatus(status.NO_CONTENT);
      }
      else {
        res.status(status.OK).send(item);
      }
    } catch (e) {
      errorHandler(e, res)
    }
  }

};

async function deleteFile(fileName) {
  //let filePath=path.resolve(path_upload, fileName);
  try {
    fileName = 'upload/' + fileName.substring(fileName.lastIndexOf("\\") + 1)
    console.log(`Deleting file ${fileName}`);
    await fs.unlink(fileName, err => {
      if (err) console.log('error' + err.message);
    });
  } catch (e) {
    console.log('error' + e.message);
  }
}

async function moveUpload(file) {

  let ext = path.extname(file.name);
  let base = path.basename(file.path);
  let pos = base.lastIndexOf('_');
  base = base.substring(pos + 1);
  let fileName = base + ext;

  path_upload = process.env.PATH_UPLOAD;
  await rename(file.path, path.resolve(path_upload, fileName));
  return fileName;
  //}
}

module.exports = userInfoController;