const request = require('request');

const getCoordination = ((location,callback) => {  //We can have space in location like "New York"
    const geocodeURL = `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(location)}.json?access_token=pk.eyJ1IjoiY2hhZGxpdSIsImEiOiJjazF5Zjl6YjEwYnd3M2RxOGxzbXVmbDVmIn0.-amrbXh46t23fJzE6WjK5w`
    request({url: geocodeURL,json: true},(error,response)=>{
        if (error){
            callback('Unable to connect to location services',undefined)
        } else if (response.body.features[0].length===0){
            callback('Unable to find location. Try another search',undefined)
        // For example, if location = "new", the API will return several possible locations in response.body.features[n] (n=0,1,2...)
        } else {
            callback(undefined, {
                longitude: response.body.features[0].center[0],
                latitude: response.body.features[0].center[1],
                location: response.body.features[0].place_name
            })
        }
    })
})

module.exports = getCoordination