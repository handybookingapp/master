require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');

const multer = require('multer');
const multerS3 = require('multer-s3')
const aws = require('aws-sdk')
const s3 = new aws.S3({
    // accessKeyId: 'AKIAZMON3HGV3QSX4MPM',
    // secretAccessKey: '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'
    region: 'ap-southeast-2',
    accessKeyId: 'AKIAI7PITPBR6ZP5SRXQ',
    secretAccessKey: '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',
})
const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'handyheroweb08',
        key: function (req, file, cb) {
            cb(null, 'avatar/' + Date.now().toString())
        }
    })
})

const userRouter = require('./routes/user');
const userInfoRouter = require('./routes/userinfo');
const taskRouter = require('./routes/task');
const offerRouter = require('./routes/offer');

const app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", ' 3.2.1')
    //console.log(req.url);
    next();
});

app.post('*', function (req, res, next) {
    //console.log('post: ', req.body);
    next();
});

// mongoose.connect(process.env.MONGODB_URL, {
//     useNewUrlParser: true,
//     useCreateIndex: true,
//     useUnifiedTopology: true
// });
mongoose.connect(process.env.MONGODB_ATLAS_URL, { useNewUrlParser: true, useCreateIndex: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("DB connected")
});

//约定好接口用复数
app.use('/users', userRouter);
app.use('/userinfos', userInfoRouter);
app.use('/tasks', taskRouter);
app.use('/offers', offerRouter);

//app.use('/upload', express.static(path.join(__dirname, 'upload')))
app.post('/upload', upload.single('file'), (req, res) => {
    res.send(req.file)
})
//console.log("Listen:" + process.env.PORT);
app.listen(process.env.PORT);