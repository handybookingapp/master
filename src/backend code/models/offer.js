const mongoose = require('mongoose');
mongoose.set('debug', true);

const Schema = mongoose.Schema;
const offerSchema = new Schema({

  quote: [{
    price: Number,
    unit: {
      type: String,
      enum: ['Hourly', 'Total'],
      default: 'Total',
    }
  }],

  createTime: { type: Date, default: Date.now },

  status: {
    type: String,
    required: true,
    enum: ['Open', 'Accepted', 'Completed', 'Withdrawn', 'Closed'],
    default: 'Open'
  },

  task: {
    type: Schema.Types.ObjectId,
    ref: 'Task',
    required: [true, 'TaskID required'],
  },

  tradie: {
    type: Schema.Types.ObjectId,
    ref: 'UserBasic',
    required: [true, 'UserID required'],
  },
});

module.exports = mongoose.model('Offer', offerSchema);