const mongoose = require('mongoose');
mongoose.set('debug', true);

const Schema = mongoose.Schema;
const taskSchema = new Schema({
	title: {
		type: String,
		required: true,
		minlength: 4,
		maxlength: 128,
	},

	//location, 判断工作距离的依据
	location: { type: String, default: 'Sydney', required: true },


	//可否remote完成。与location并不冲突
	allowRemote: {
		type: Boolean, default: false, required: true
	},

	budget: {
		type: Number,
		required: true,
	},

	detail: {
		type: String,
		required: true,
		minlength: 4,
		maxlength: 4096
	},

	dueDate: { type: Date },

	//原来是created time, 考虑可以修改,所以改名变成last updated time
	lastUpdatedTime: { type: Date, default: Date.now, required: true },
	//stored as url, TODO: need some validation for the length of the aray.
	photos: {
		urls: [{ type: String }],
	},

	//anything 对于task来说就是所有cat都行。
	category: {
		type: String,
		required: true,
		enum: ["Cleaning", "Business & Admin", "Delivery & Removals", "Furniture Assembly", "Handyman", "Marketing & Design", "Home & Gardening", "Something else"
		],
		default: 'Something else'
	},

	status: {
		type: String,
		required: true,
		enum: ['Open', 'Assigned', 'Completed', 'Cancelled', 'Expired'],
		default: 'Open'
	},

	user: {
		type: Schema.Types.ObjectId,
		ref: 'UserBasic',
		required: [true, 'UserID required'],
	},

	userinfo: {
		type: Schema.Types.ObjectId,
		ref: 'UserInfo',
		required: [true, 'UserInfoID required'],
	}

});




taskSchema.statics.findByUserID = async function (user) {

	const items = await this.find({ user }).populate('userinfo')
		.sort({ createTime: -1 })
		.exec();

	if (!items) {
		//console.log('No task yet');
		return;
	}
	return items;
};

taskSchema.statics.findByCategoryAndTitle = async function (category, title) {

	const query = {};
	if (category !== undefined) query.category = category;
	if (title !== undefined) query.title = new RegExp(title, 'i');

	const items = await this.find(query).populate('userinfo')
		.sort({ createTime: -1 })
		.exec();
	//.limit(10)
	//select({ name: 1, dueDate: 1 })

	if (!items) {
		//console.log('No task yet');
		return;
	}
	return items;
};

module.exports = mongoose.model('Task', taskSchema);