const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');

const userBasicSchema = new Schema(
	{
		email: {
			type: String,
			required: [true, 'Email required'],
			unique: [true, 'Email already registered'],
			minlength: 6,
			maxlength: 32,
			validate: email => /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)
		},
		password: {
			type: String,
			minlength: [4, 'Too short'],
			maxlength: [32, 'Too long']
		},
		active: { type: Boolean, default: true },
		createTime: { type: Date, default: Date.now },
	}
);

userBasicSchema.pre('save', async function (next) {
	const user = this;
	if (user.isModified('password')) {
		user.password = await bcrypt.hash(user.password, 8);
	}
	next();
});

userBasicSchema.statics.findByEmailPassword = async function (email, password) {
	const user = await this.findOne({ email }).exec();
	if (!user) {
		//console.log('no such Email');
		return;
	}
	if (await bcrypt.compare(password, user.password)) {
		return user;
	} else {
		//console.log('wrong password');
		return;
	}
};

userBasicSchema.methods.generateJwt = function () {
	const user = this;
	return jwt.sign({
		_id: user.id,
		email: user.email
	},
		process.env.JWT_SECRET,
		{ expiresIn: parseInt(process.env.JWT_EXPIRATION) }
	);
};

module.exports = mongoose.model('UserBasic', userBasicSchema);