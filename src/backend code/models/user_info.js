const mongoose = require('mongoose');
mongoose.set('debug', true);

const Schema = mongoose.Schema;
const userInfoSchema = new Schema(
  {

    user: {
      type: Schema.Types.ObjectId,
      ref: 'UserBasic',
      required: [true, 'UserID required'],
      unique: [true, 'UserID duplicate is not allowed'],
    },

    firstName: {
      type: String,
      required: [true, 'First name required'],
      minlength: [2, 'At least 2 letters'],
      maxlength: [32, 'Should less than 32 letters']
    },

    lastName: {
      type: String,
      required: [true, 'Last name required'],
      minlength: [2, 'At least 2 letters'],
      maxlength: [32, 'Should less than 32 letters']
    },

    //TODO: this url needs to be chenged
    avatar: {
      type: String,
      default: 'https://gladstoneentertainment.com/wp-content/uploads/2018/05/avatar-placeholder.gif'
    },
    reviews: [{

      type: String,

    }],
    //Leon question: change it to comments array??
    commonds: {
      type: String,
    },

    mobileNumber: {
      type: Number,
    },
    email: {
      type: String,
    },

    //Leon question: change it to comment?? not needed, size is implied by comments array
    commondsNumber: {
      type: Number,
    },

    birthday: {
      type: String,
    },

    tagline: {
      type: String,
    },

    //Leon question: what is the length??
    abn: {
      type: Number,
      length: [11]
    },

    description: {
      type: String,
    },

    languages: {
      type: String,
    },

    qualifications: {
      type: String,
    },

    workExperience: {
      type: String,
    },

    location: { type: String },

    //we do not need this for now
    isTradie: { stype: Boolean, default: false },

    postTasks: {
      type: Boolean
    },
    earnMoney: {
      type: Boolean
    },
    // category: {
    //   type: String,
    //   enum: ['Cleaning', 'Business & Admin', 'Delivery & Removals', 'Furniture & Assembly', 'Handyman', 'Marketing & Design', 'Home & Gardening', 'Something else', 'Anything'],
    // }

    categories: [{

      // category:{
      type: String,
      //  enum:['Cleaning', 'Business & Admin', 'Delivery & Removals', 'Furniture & Assembly', 'Handyman', 'Marketing & Design', 'Home & Gardening', 'Something else', 'Anything']
      // }
    }],

  }
);



//TODO：添加其它task可能需要的有关user 信息。此方法属于备用方法，不一定需要
userInfoSchema.virtual('taskRelatedDetails').get(function () {
  let fullName = this.firstName + ' ' + this.lastName;
  return ({ fullName, location: this.location, avatar: this.avatar });
});
userInfoSchema.statics.findTaskRelatedDetailsByUser = async function (user) {
  const item = await this.findOne({ user }).exec()
  console.log(item)
  return item.taskRelatedDetails;
}

//找的时候没有用categories, 还是根据category找的。找出来的user的categories会不对。
// userInfoSchema.statics.findByCategory = async function (category) {
//   const items = await this.find({ category }).exec()
//   //console.log(items)
//   return items
// }
userInfoSchema.statics.findByCategory = async function (category) {
  const users = await this.find({}).exec();
  let results = [];
  users.map((user) => {
    if (user.categories.includes(category)) {
      // console.log("++++\n\n\n" + user + "++++\n\n\n");
      results.push(user);
    }
  });
  // console.log(results)
  return results;
}

module.exports = mongoose.model('UserInfo', userInfoSchema);