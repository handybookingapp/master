const app = require('express');
const jwt = require('express-jwt');
const offerRouter = app.Router();

const itemController = require('../controllers/offer');

offerRouter.get('/', itemController.index);
offerRouter.get('/:id', jwt({ secret: process.env.JWT_SECRET }), itemController.show);
offerRouter.post('/', jwt({ secret: process.env.JWT_SECRET }), itemController.create);
offerRouter.put('/:id', jwt({ secret: process.env.JWT_SECRET }), itemController.update);
offerRouter.delete('/:id', jwt({ secret: process.env.JWT_SECRET }), itemController.delete);

module.exports = offerRouter;