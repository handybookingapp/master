const app = require('express');
const jwt = require('express-jwt');
const taskRouter = app.Router();

const itemController = require('../controllers/task');

taskRouter.get('/', itemController.index);
// taskRouter.get('/id/:id', jwt({ secret: process.env.JWT_SECRET }), itemController.show);
// taskRouter.get('/:url', jwt({ secret: process.env.JWT_SECRET }), itemController.showByTitle);
// taskRouter.get('/user/:user', jwt({ secret: process.env.JWT_SECRET }), itemController.findByUserID);


taskRouter.get('/id/:id', itemController.show);
taskRouter.get('/user/:user', itemController.findByUserID);
//这个是专门处理从tasklist发出的请求，匹配有相关filter和无filter的url
taskRouter.get('(/category/:category?)?(/title/:title?)?', itemController.findByCategoryAndTitle);

taskRouter.get('/:url', itemController.showByTitle);

taskRouter.post('/', itemController.create);
taskRouter.put('/:id', itemController.update);
taskRouter.delete('/:id', itemController.delete);
// taskRouter.delete('/:id', jwt({ secret: process.env.JWT_SECRET }), itemController.delete);

module.exports = taskRouter;