const app = require('express');
const jwt = require('express-jwt');
const userRouter = app.Router();

const userController = require('../controllers/user');

userRouter.get('/', userController.index);
userRouter.get('/:id/', userController.show);
userRouter.post('/', userController.create);
userRouter.put('/:id', jwt({ secret: process.env.JWT_SECRET }), userController.update);
userRouter.delete('/:id', jwt({ secret: process.env.JWT_SECRET }), userController.delete);
userRouter.post('/login', userController.login);
module.exports = userRouter;