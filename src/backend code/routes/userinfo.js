const app = require('express');
const jwt = require('express-jwt');
const multer = require('multer');//接收图片
const upload = multer({
    dest: './upload'
})
// const aws = require('aws-sdk')
// const s3 = new aws.S3({
//     // accessKeyId: 'AKIAZMON3HGV3QSX4MPM',
//     // secretAccessKey: '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'
//     region: 'ap-southeast-2',
//     accessKeyId: 'AKIAI7PITPBR6ZP5SRXQ',
//     secretAccessKey: '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',
// })

// const upload = multer({
//     storage: multerS3({
//         s3: s3,
//         bucket: 'handyheroweb08',
//         key: function (req, file, cb) {
//             cb(null, 'aaaa/' + Date.now().toString())
//         }
//     })
// })

const userInfoRouter = app.Router();

const userInfoController = require('../controllers/userinfo');
//userinfo 的id不会被暴露，会使用userbasic的id ?
userInfoRouter.get('/', userInfoController.index);
userInfoRouter.get('/:basicId', userInfoController.showByBasicID);
userInfoRouter.get('/category/:category', userInfoController.findByCategory);

userInfoRouter.post('/', jwt({ secret: process.env.JWT_SECRET }), userInfoController.create);
userInfoRouter.post('/avatar/', upload.single('file'), userInfoController.uploadAvatar);

userInfoRouter.put('/:basicId', userInfoController.updateByBasicID);

userInfoRouter.delete('/:basicId', jwt({ secret: process.env.JWT_SECRET }), userInfoController.deleteByBasicID);

//jwt({ secret: process.env.JWT_SECRET }),
module.exports = userInfoRouter;