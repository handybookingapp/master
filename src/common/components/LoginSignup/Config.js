export const Category = [
	{ label: 'Bookeeping', url: 'book' },
	{ label: 'BAS Accounting', url: 'acc' },
	{ label: 'Budgeting Help', url: 'budget' },
	{ label: 'Financial Modelling', url: 'fin' },
	{ label: 'Finacial Planning', url: 'plan' },
	{ label: 'Moving', url: 'move' },
	{ label: 'Something else', url: 'anything' }
]