import React, { Component } from 'react';
import './Password.scss';

class Password extends Component {

    checkPwd(e) {
        let v = e.target.value;
        let num = this.pwdChange(v);
        let tds = document.getElementsByClassName("pwd-levelBar");
        let tdLevel = document.getElementsByClassName("pwd-levelCap");
        tds[0].style = tds[1].style = tds[2].style = 'display: block';

        if (num === 0 || num === 1) {
            tds[0].style.backgroundColor = tdLevel[0].style.color = "red";
            tds[1].style.backgroundColor = "gray";
            tds[2].style.backgroundColor = "gray";
            tdLevel[0].innerText = 'Poor';
        }
        else if (num === 2) {
            tds[0].style.backgroundColor = tdLevel[0].style.color = "orange";
            tds[1].style.backgroundColor = "orange";
            tds[2].style.backgroundColor = "gray";
            tdLevel[0].innerText = "Middle";
        }
        else if (num === 3) {
            tds[0].style.backgroundColor = tdLevel[0].style.color = "green";
            tds[1].style.backgroundColor = "green";
            tds[2].style.backgroundColor = "green";
            tdLevel[0].innerText = "Strong";
        }
        else {
            tds[0].style = tds[1].style = tds[2].style = 'display: none';
            tdLevel[0].innerText = ' ';
        }
    }

    pwdChange(v) {
        var num = 0;
        var reg = /\d/;
        if (reg.test(v)) {
            num++;
        }
        reg = /[a-zA-Z]/;
        if (reg.test(v)) {
            num++;
        }
        reg = /[^0-9a-zA-Z]/;
        if (reg.test(v)) {
            num++;
        }

        if (v.length < 4) {
            num--;
        } else {
            this.props.getPassword(v);
        }
        return num;
    }

    render() {
        return (
            <div className='pwd'>
                <input type="password" id="txtPwd" placeholder="Password"
                    onKeyUp={this.checkPwd.bind(this)}
                    className={this.props.inputStyle || 'pwd-input'} />
                <div className='pwd-levelBars'>
                    <div className='pwd-levelBar'></div>
                    <div className='pwd-levelBar'></div>
                    <div className='pwd-levelBar'></div>
                    <div className='pwd-levelCap'></div>
                </div>
            </div>
        )
    }
}

export default Password;