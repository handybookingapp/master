import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import userAPI from '../../../services/userAPI';
import Password from './Password';
// import postData from '../../Util';
import './Signup.scss';

class SignUp extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: '',
			checked: true,
			created: false,
			duplicated: false,
		}
	}

	userChange(e) {
		this.setState({
			email: e.target.value,
			checked: true,
			duplicated: false,
		})
	}

	pwdChange(pwd) {
		this.setState({
			password: pwd,
			checked: true,
		})
	}

	async submit(e) {
		e.preventDefault();
		const { email, password } = this.state;
		//console.log(email + '  /  ' + password);
		//let history = this.context.router.history;

		if (this.validate(email, password)) {

			try {
				let data = await userAPI.createUser({ email, password });
				let storage = window.localStorage;
				storage.setItem('token', data.token);
				this.setState({ created: true });
				//console.log(storage.token);
			}
			catch (error) {
				console.log(error);
				this.setState({
					created: false
				})

				if (error.message.indexOf("409") != -1) {
					this.setState({
						duplicated: true,
					})
				} else {
					this.setState({
						checked: false,
					})
				}
			};
		}
		else {
			//invalid input
			this.setState({
				checked: false
			})
		}
	}

	validate(user, pwd) {
		const exp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (user.length < 4 || user.length > 32 || !exp.test(user)) {
			//console.log('invalid email');
			return false;
		}

		if (pwd.length < 4 || pwd.length > 32) {
			//console.log('invalid password');
			return false;
		}
		return true;
	}

	render() {
		let current = this.props.location.pathname.replace("/sign-up", "");

		if (this.state.created) {
			return (
				<Redirect to={current + "/sign-up-step2"} />
			)
		}

		return (
			<div className="pop-up-form">

				<div className="cover">
					<div className="container">
						<div className="header_wrapper">
							<div className="header_title">Join us
								<Link to={current}>
									<svg tabIndex="0" className="close_logo" width="24" height="24" viewBox="0 0 24 24">
										<path d="M13.17 12l6.41-6.42a.82.82 0 0 0-1.16-1.16L12 10.83 5.58 4.42a.82.82 0 0 0-1.16 1.16L10.83 12l-6.41 6.42a.8.8 0 0 0 0 1.16.8.8 0 0 0 1.16 0L12 13.17l6.42 6.41a.8.8 0 0 0 1.16 0 .8.8 0 0 0 0-1.16z">
										</path>
									</svg>
								</Link>
							</div>
						</div>

						<main>
							<div className="signup-form" onSubmit={this.submit.bind(this)}>
								<form className="login-form">

									<div className="login-form__input">
										<label forhtml="label-email" className="label__email">Email</label>
										<div className="input-item">
											<input name="email" id="label-email" placeholder="Email" type="email"
												className={!this.state.checked ? 'input-email red-box' : 'input-email'} onChange={this.userChange.bind(this)} />
											{!this.state.checked && <span className="error-tip">Please input correct email address</span>}
											{this.state.duplicated && <span className="error-tip">Email already registered</span>}
										</div>
									</div>

									<div className="login-form__input">
										<label forhtml="label-pwd" className="label__email">Password</label>
										<div className="input-item">
											<Password
												inputStyle={'input-email ' + (!this.state.checked ? 'red-box' : '')}
												getPassword={pwd => this.pwdChange(pwd)}
											/>
											{!this.state.checked && <span className="error-tip">Please input correct password</span>}
										</div>
									</div>

									<button className="botton_join" type="submit">Join HandyBooking</button>

									<div className="separator">
										or sign up with
                  					</div>

									<div className="box-social-buttons">
										<button className="facebook_button">
											<div className="facebook_button_icon">
												<svg width="24" height="24" viewBox="0 0 24 24">
													<path d="M12 2a10 10 0 0 0-1.56 19.88v-7H7.9V12h2.54V9.8a3.52 3.52 0 0 1 3.77-3.89 15.72 15.72 0 0 1 2.24.19v2.46h-1.26a1.45 1.45 0 0 0-1.63 1.56V12h2.78l-.45 2.89h-2.33v7A10 10 0 0 0 12 2z">
													</path>
												</svg>
											</div>
											Facebook
                    					</button>

										<div className="box"></div>

										<button className="google_button">
											<div className="google_button_icon">
												<svg width="24" height="24" viewBox="0 0 24 24" fill="none">
													<path d="M22 12.23c0-.707-.063-1.393-.183-2.045h-9.613v3.871h5.491c-.235 1.253-.957 2.31-2.035 3.017v2.511h3.296C20.888 17.837 22 15.275 22 12.23z" fill="#3E82F1"></path><path d="M12.204 22c2.757 0 5.067-.893 6.752-2.421l-3.296-2.512c-.911.602-2.08.956-3.456.956-2.66 0-4.907-1.759-5.71-4.124h-3.41v2.595C4.763 19.76 8.213 22 12.203 22z" fill="#32A753"></path><path d="M6.494 13.899A5.848 5.848 0 0 1 6.174 12c0-.657.114-1.298.32-1.899v-2.59h-3.41A9.863 9.863 0 0 0 2 12c0 1.612.396 3.14 1.083 4.489l3.411-2.59z" fill="#F9BB00"></path><path d="M12.204 5.978c1.496 0 2.843.505 3.898 1.494l2.929-2.87C17.265 2.988 14.955 2 12.204 2c-3.99 0-7.44 2.242-9.12 5.511l3.41 2.59c.803-2.365 3.05-4.123 5.71-4.123z" fill="#E74133">
													</path>
												</svg>
											</div>
											Google
                    					</button>
									</div>
								</form>

								<div className="tips_set">
									<div type="checkbox" focusable="true" className="InputOption">
										<svg className="InputOption__Tick" fill="none" height="8" viewBox="0 0 12 8" width="12">
											<path d="M9.994.304a.75.75 0 1 1 1.063 1.057L4.758 7.696a.75.75 0 0 1-1.096-.035L.91 4.513a.75.75 0 1 1 1.13-.987l2.222 2.542z">
											</path>
										</svg>
									</div>
									<p className="text">
										Please don't send me tips or marketing via email or sms.
                  </p>
								</div>

								<p className="text">By signing up, I agree to HandyBooking's
                  <a target="_blank" className="sign_up_form__StyledLink" href="/terms/"> Terms &amp; Conditions</a>,
                  and <a target="_blank" className="sign_up_form__StyledLink" href="/community-guidelines/">Community Guidelines</a>.
                  <a target="_blank" className="sign_up_form__StyledLink" href="/privacy/">Privacy Policy</a>.
                </p>

								<div className="footer">
									<span>Already have an account ?</span>
									<Link to={current + "/login"}>
										<button className="button_login">Log in</button>
									</Link>
								</div>
							</div>
						</main>
					</div>
				</div>

			</div>
		);
	}
}

export default SignUp;