import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { getCurrentID, getData } from '../../../Util'
import userAPI from '../../../services/userAPI';
import './Signup.scss';

class SignUp2 extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			firstName: '',
			lastName: '',
			location: '',
			suburbSelected: false,
			initial: true,
			checked: true,
			created: false,
			expired: false,
			suburbList: [],
		}
	}

	// componentDidMount() {
	// 	let token = window.localStorage.getItem('token');
	// 	let payload = jwt_decode(token);
	// 	//console.log('payload ' + payload);
	// 	//console.log('email ' + payload.email);
	// }

	valueChange(e) {
		this.setState({
			[e.target.name]: e.target.value,
			checked: true,
		})
	}

	matchSuburb(e) {
		this.setState({
			initial: false,
		})
		let inputSuburb = e.target.value;

		//start query when input 3 digits
		if (inputSuburb.length < 3) {
			return;
		}

		const url = 'https://cors-anywhere.herokuapp.com/http://v0.postcodeapi.com.au/suburbs.json?name=ahttp://v0.postcodeapi.com.au/suburbs.json';

		let datalist = '';

		const param = {
			name: e.target.value,
		};
		this.setState({
			suburbSelected: true,
			location: inputSuburb,
		})
		// console.log(inputSuburb)

		getData(url, param)
			.then(res => {
				datalist = res.data;
				console.log(datalist);
				this.setState({
					suburbList: datalist
				})
			})
			.catch(err => {
			})
		//change color if choose one from datalist
		// this.setState({
		// 	location: e.target.value,
		// })
	}

	submit(e) {
		e.preventDefault();
		if (this.validate(this.state.firstName, this.state.lastName) && this.state.suburbSelected) {
			const param = {
				...this.state,
				user: getCurrentID(),
			};
			//console.log(param);

			userAPI.createDetail(param)
				.then(data => {
					//console.log(data);
					this.setState({
						data,
						isLoading: false,
						created: true,
					});
					let currentPath = document.location.pathname;
					let redirectURL = currentPath.replace("/sign-up-step2", "");
					document.location.href = redirectURL;
				})
				.catch(error => {
					//console.log(error);
					if (error.response.status === 401) {
						this.setState({
							expired: true,
						})
					}
				});
		}
		else {
			//invalid input
			this.setState({
				checked: false
			})
		}
	}

	validate(firstName, lastName) {
		if (firstName.length < 2 || lastName.length < 2 || lastName.length > 32 || firstName.length > 32) {
			//console.log('Name length should between 2 and 32');
			return false;
		}
		return true;
	}

	render() {
		if (this.state.created) {
			return (
				<Redirect to="/" />
			)
		}

		if (this.state.expired) {
			return (
				<Redirect to="/logout" />
			)
		}

		return (
			<div className="pop-up-form">
				<div className="cover">
					<div className="container">
						<div className="header_wrapper">
							<div className="header_title">Welcome to HandyHero!
                				<Link to="/" onClick={this.props.logOut}>
									<svg tabIndex="0" className="close_logo" width="24" height="24" viewBox="0 0 24 24">
										<path d="M13.17 12l6.41-6.42a.82.82 0 0 0-1.16-1.16L12 10.83 5.58 4.42a.82.82 0 0 0-1.16 1.16L10.83 12l-6.41 6.42a.8.8 0 0 0 0 1.16.8.8 0 0 0 1.16 0L12 13.17l6.42 6.41a.8.8 0 0 0 1.16 0 .8.8 0 0 0 0-1.16z">
										</path>
									</svg>
								</Link>
							</div>
						</div>

						<main>
							<p className="text">
								Tell us about yourself to setup your profile
              				</p>

							<div className="signup_form">
								<form className="login_form" onSubmit={this.submit.bind(this)}>
									<div className="login-form__input">
										<label forhtml="label-fName" className="label_email">First Name</label>
										<div className="input-item">
											<input name="firstName" id="label-fName" placeholder="First name" type="text"
												className={!this.state.checked ? 'input_email red_box' : 'input_email'} onChange={this.valueChange.bind(this)} />
											{!this.state.checked && <span className="error_tip">Please input correct name</span>}
										</div>
									</div>

									<div className="login-form__input">
										<label forhtml="label-lName" className="label_email">Last Name</label>
										<div className="input-item">
											<input name="lastName" id="label-lName" placeholder="Last name" type="text"
												className={"input_email " + (!this.state.checked ? 'red_box' : '')} onChange={this.valueChange.bind(this)} />
											{!this.state.checked && <span className="error_tip">Please input correct name</span>}
										</div>
									</div>

									<div className="login-form__input">
										<label forhtml="label-sub" className="label_email">Enter your home suburb</label>
										<div className="input-item">
											<input name="location" id="label-sub" placeholder="Suburb of your address" type="text"
												className={"input-email "
													+ (!this.state.checked ? 'red-box ' : '')
													+ (this.state.suburbSelected ? 'blue-font' : '')
												}
												onChange={this.matchSuburb.bind(this)}
												list="suburb" />
											<datalist id="suburb">
												{this.state.suburbList.map((item, index) =>
													<option key={index} value={`${item.name} ${item.state.abbreviation} ${item.postcode}`} />)}
											</datalist>

											{!this.state.initial && !this.state.suburbSelected && <span className="error-tip">Please select correct suburb</span>}
										</div>
									</div>

									{/* <p className="text">
                    What would you like to use HandyHero for?
                  </p>
                  <div className="tips_set">
                    <div type="checkbox" focusable="true" className="InputOption">
                      <svg className="InputOption__Tick" fill="none" height="8" viewBox="0 0 12 8" width="12">
                        <path d="M9.994.304a.75.75 0 1 1 1.063 1.057L4.758 7.696a.75.75 0 0 1-1.096-.035L.91 4.513a.75.75 0 1 1 1.13-.987l2.222 2.542z">
                        </path>
                      </svg>
                    </div>
                    <p className="text">
                      I want to get things done
                    </p>

                    <div type="checkbox" focusable="true" className="InputOption">
                      <svg className="InputOption__Tick" fill="none" height="8" viewBox="0 0 12 8" width="12">
                        <path d="M9.994.304a.75.75 0 1 1 1.063 1.057L4.758 7.696a.75.75 0 0 1-1.096-.035L.91 4.513a.75.75 0 1 1 1.13-.987l2.222 2.542z">
                        </path>
                      </svg>
                    </div>
                    <p className="text">
                      I want to earn money
                    </p>
                  </div> */}

									<button className="botton_join" ref="btn" type="submit">Continue</button>
								</form>

							</div>
						</main>
					</div>
				</div>
			</div>
		);
	}
}

export default SignUp2;