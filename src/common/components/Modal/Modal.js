import React from 'react';
import './modal.scss';
import { getReturnPath } from '../../common';
import { Link } from 'react-router-dom';
class Modal extends React.Component {

  componentDidMount() {
    this.returnPath = getReturnPath();
  }


  render() {

    return (

      <div className="Modal Modal-cover">
        <div className="Modal-content">
          <div className="Modal-content-top">
            <h1>{this.props.title}</h1>
            <Link className="exit-button icon-close" to={this.returnPath}>
            </Link>
          </div>
          <React.Fragment>
            {this.props.children}
          </React.Fragment>
        </div>
      </div >
    );
  }
}

export default Modal;
