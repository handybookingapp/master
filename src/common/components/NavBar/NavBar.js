
import React from 'react';
import { withRouter, Link, NavLink } from 'react-router-dom';

import logo from './logo.png';
import logoWhite from './logo-white.png';
// import avatar from '../images/avatar.png';
import './navBar.scss';
import avatarPlaceHolder from '../../../common/images/avatar-placeholder.gif'

class NavBar extends React.Component {
  constructor() {
    super();
  }

  componentDidMount() {

    window.addEventListener('scroll', this.handleScrollOrResize);
    window.addEventListener('resize', this.handleScrollOrResize);
    this.logoImages = document.querySelectorAll('.logo');
    this.nav = document.querySelector('.navigation-bar');
    this.dropdownContent = document.querySelector('.dropdown-content');
    this.dropdownContent.classList.add("display-none");

  }

  componentDidUpdate(prevProps) {
    // //console.log(prevProps);
    //console.log(this.props);

    let path = this.props.currentPath;
    // let signUpRegex = /.*(\/sign-up)$/;
    // let loginRegex = /.*(\/login)$/;
    this.handleScrollOrResize();

    if ((path === '/' || path === '/sign-up' || path === '/login') && window.scrollY === 0) {
      this.addTrans();
      // //console.log("did update addtrans")

    } else if (path !== undefined) {
      this.removeTrans();
      // //console.log("did update removetrans")
    }
  }


  handleScrollOrResize = () => {
    let distanceFromTop = window.scrollY;
    let width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if (distanceFromTop < 60 && this.props.currentPath === "/" && width > 682) {
      this.addTrans();
    } else {
      this.removeTrans();
    }

  }

  removeTrans = () => {
    this.logoImages.forEach((image) => image.src = logo);
    this.nav.classList.remove('transparent');
  }

  addTrans = () => {
    let width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if (width > 640) this.logoImages.forEach((image) => image.src = logoWhite);
    this.nav.classList.add('transparent');
  }



  UserButtons = ({ currentUser, currentPath }) => {
    //console.log(currentUser);
    return (
      (currentUser === null) ?
        <>
          <li className="user-button"><Link to={currentPath === "/" ? "/sign-up" : currentPath + "/sign-up"}>Sign up</Link></li>
          <li className="user-button"><Link to={currentPath === "/" ? "/login" : currentPath + "/login"}>Log in</Link></li>
        </>
        :
        <>
          <li className="user-button">
            <NavLink to="/privateuserprofile" >
              <img className="avatar" alt="avatar" src={currentUser.avatar} onError={(e) => { e.target.onerror = null; e.target.src = avatarPlaceHolder }} />


            </NavLink>

          </li>
          <li className="user-button"><Link onClick={this.props.logOut} to={"/"}>Log out</Link></li>
        </>
    )
  }

  handleDropdownClick = () => {
    // //console.log(this.dropdownContent);
    if (this.dropdownContent.classList.contains("display-none")) {
      this.dropdownContent.classList.remove("display-none");
    } else {
      this.dropdownContent.classList.add("display-none");
    }
  }
  render() {

    //console.log(this.props);
    let currentPath = this.props.currentPath;
    let currentUser = this.props.currentUser;
    let UserButtons = this.UserButtons;
    const PostButton = () => (<><li><NavLink className="post-button" to={currentPath === "/" ? "/post-task" : currentPath + "/post-task"}>Post a task</NavLink></li></>);

    return (
      <nav className='navigation-bar transparent' >

        <ul className="top-bar">
          <div className="left">
            <li className="logo-link"><Link to="/"><img src={logoWhite} className="logo" alt="logo" /></Link></li>
            {/* post a task is only available when you haved logged in */}
            <PostButton />
            <li><NavLink to="/tasks" onClick={this.props.loadTasks}> Browse tasks</NavLink></li>
            <li><NavLink to="/workers" onClick={this.props.loadUsers}>Our heroes</NavLink></li>
          </div>
          <div className="right">

            <UserButtons currentUser={currentUser} currentPath={currentPath} />

          </div>
        </ul >

        <span className="mini-bar">
          <ul>
            <li className="logo-container">
              <div className="logo-link"><Link to="/"><img src={logoWhite} className="logo" alt="logo" /></Link></div>
            </li>
            <UserButtons currentUser={currentUser} currentPath={currentPath} />

            <li className="drop-button-area">
              <button className=" drop-button" onClick={this.handleDropdownClick}>
                <i className="fas fa-bars"></i>
              </button>
            </li>
          </ul>

          <ul className="dropdown-content">
            {/* post a task is only available when you haved logged in */}
            <PostButton />
            <li><NavLink onClick={this.handleDropdownClick} to="/tasks">Browse tasks</NavLink></li>
            <li><NavLink onClick={this.handleDropdownClick} to="/workers" >Our heroes</NavLink></li>
          </ul>
        </span>

      </nav >
      //need to make another navbar for small screen size
    );
  }
}
export default NavBar;
