import React from 'react';
import { taskCategories } from '../../common';
import { isObjectEmpty } from '../../../common/common';
import moment from 'moment';
import taskAPI from '../../../services/taskAPI'
import Modal from '../../../common/components/Modal/Modal'
import './postTask.scss';
class PostTask extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            displayPage: 1,
            //new task info entered by user
            task: {
                category: 'Cleaning',
                title: '',
                detail: '',
                location: '',
                allowRemote: false,
                dueDate: '',
            }
        }
        this.lastStep = "Post";
    }

    //wait for props to be ready
    componentDidMount() {
        let task;

        if (isObjectEmpty(this.props.task)) {
            task = this.state.task;
        }
        else {//either copy task or exdit
            task = JSON.parse(JSON.stringify(this.props.task));//deep clone

            if (this.props.edit) {
                this.lastStep = "Update";
            } else {
                delete task['_id'];
            }
        }

        if (!isObjectEmpty(this.props.currentUser)) {
            task.user = this.props.currentUser.user;
            task.userinfo = this.props.currentUser._id;
        }

        this.setState({ task });

    }


    nextPage = () => {
        let displayPage = this.state.displayPage + 1;
        this.setState({
            displayPage
        });
    }
    previousPage = () => {
        let displayPage = this.state.displayPage - 1;
        this.setState({
            displayPage
        });
    }

    postTask = (e) => {
        // console.log("post task", this.state.task);
        e.preventDefault();

        let action;

        if (this.props.edit) {
            action = taskAPI.updateTask;
        } else {
            action = taskAPI.createTask;
        }
        try {
            action(this.state.task)
                .then((r) => console.log("post task", r))
                .then((r) => document.location.href = this.fullReturnPath);

        } catch (e) {
            console.log(e);
        }

    }
    changeRemote = () => {
        let task = this.state.task;
        task.allowRemote = !task.allowRemote;
        this.setState({ task });
    }


    render() {

        return (

            <Modal title={"What would you like to " + this.lastStep.toLowerCase() + "?"} >

                {this.state.displayPage === 1 &&
                    <div className="firstpage" >

                        <p>I need</p >
                        <select
                            value={this.state.task.category}
                            onChange={(e) => {
                                let task = this.state.task;
                                task.category = e.target.value;
                                this.setState({ task });
                            }} >

                            {Object.values(taskCategories).map((v) =>
                                v !== 'Anything' && <option key={v} value={v}> {v} </option>
                            )}
                        </select> <br />
                        <div className="navigation">
                            <button className="button button--green" onClick={this.nextPage}>Next</button>
                        </div>
                    </div>

                }

                {this.state.displayPage === 2 &&

                    <div className="secondpage">
                        <form>
                            <p> What do you need done?</p >
                            <label htmlFor="need" >This'll be the title of your task</label>
                            <input type="text" id="need" name="need" placeholder="eg. I need accounting services..."
                                value={this.state.task.title}
                                onChange={(e) => {
                                    let task = this.state.task;
                                    task.title = e.target.value;
                                    this.setState({ task });
                                }}
                            />
                            <p>What are the details?</p >
                            <label htmlFor="detail" >Be as specific as you can about what needs doing</label>

                            <textarea
                                value={this.state.task.detail}
                                onChange={(e) => {
                                    let task = this.state.task;
                                    task.detail = e.target.value;
                                    this.setState({ task });
                                }}
                                style={{ height: "10rem" }} ></textarea>

                        </form>
                        <div className="navigations">
                            <button onClick={this.previousPage}>Back</button>
                            <button onClick={this.nextPage}>Next</button>
                        </div>
                    </div>
                }

                {this.state.displayPage === 3 &&

                    <div className="thirdpage" >

                        <form >
                            <label htmlFor="location" >Location</label>
                            <input type="text" id="location" name="location" value={this.state.task.location}
                                onChange={(e) => {
                                    let task = this.state.task;
                                    task.location = e.target.value;
                                    this.setState({ task });
                                }}
                            />

                            <label htmlFor="remote" >Can be done remotely?</label>
                            <input type="checkbox" checked={this.state.task.allowRemote} value={this.state.task.allowRemote}
                                onChange={(e) => {
                                    this.changeRemote()
                                }}
                            /><br />

                            <label htmlFor="date" >When do you need it done?</label><br />
                            <input type="date" id="date" name="date" min={moment(Date.now()).format("YYYY-MM-DD")} value={moment(this.state.task.dueDate).format('YYYY-MM-DD')}
                                onChange={(e) => {
                                    let task = this.state.task;
                                    task.dueDate = e.target.value;
                                    this.setState({ task });
                                }}
                            /><br />
                            <label htmlFor="budget" >What is your budget?</label>
                            <p>Please enter the price you're comfortable to pay to get
                        your task done. Taskers will use this as a guide for how much to offer.</p >
                            <input type="text" id="budget" name="budget" value={this.state.task.budget}
                                onChange={(e) => {
                                    let task = this.state.task;
                                    task.budget = e.target.value;
                                    this.setState({ task });
                                }}
                            />
                        </form>
                        <div className="navigations">
                            <button onClick={this.previousPage}>Back</button>
                            <button onClick={this.postTask} className="post-button">{this.lastStep}</button>
                        </div>
                    </div>
                }


            </Modal>

        );
    }
}

export default PostTask;