import React from 'react';
import { NavLink } from 'react-router-dom';
import { taskCategories } from '../../common';
import './servicePicker.scss';
export const ServicePicker = ({ loadUsers }) =>
  <div className="categories">
    {
      Object.values(taskCategories).map((c) => {
        return < NavLink className="category" onClick={() => loadUsers({ categories: c })} key={Object.values(taskCategories).indexOf(c)} to={`/workers/${c}`}> {c} </NavLink>
      })
    }
  </div>

export default ServicePicker;

