import SignUp from './components/LoginSignup/SignUp';
import SignUp2 from './components/LoginSignup/SignUp2';

import Login from './components/LoginSignup/Login';
import Logout from './components/LoginSignup/Logout';

import NavBar from './components/NavBar/NavBar';
import ServicePicker from './components/ServicePicker/ServicePicker';

import PostTask from './components/PostTask/PostTask';


export default { Logout, SignUp2, SignUp, Login, NavBar, ServicePicker, PostTask };
export { Logout, SignUp2, SignUp, Login, NavBar, ServicePicker, PostTask };