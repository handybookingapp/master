import React from 'react';
import { ServicePicker } from '../../common';
import TasksBanner from './TasksBanner';
import TopWorkers from './TopWorkers';

import './homePage.scss';

class HomePage extends React.Component {


  // //console.log(tasks);
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {

    return (
      <div className="home-page">
        <div className="image">
          <div>
            <span>The best person for the job isn't always who you think</span>
            <span>Find the people with the skills you need on HandyHero</span>
          </div>
        </div>

        <h1>Need something done? Hire our heroes and set your hands free!</h1>
        <ServicePicker loadUsers={this.props.loadUsers} />

        {/* RecentlyCompletedTasks=========================================== */}
        <TasksBanner tasks={this.props.tasks} loadTasks={this.props.loadTasks} />

        {/* Top Workers=========================================== */}
        <h1>Give a hand to others, be honoured! Join us today!</h1>
        {/* Todo: should pass props to Worker */}
        {/* <TopWorkers users={this.props.users} loadUsers={this.props.loadUsers} /> */}
      </div>


    )
  }
}
export default HomePage
