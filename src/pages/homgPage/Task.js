import React from "react";
// import { taskCategories } from "../../common/common.js";
import { Link } from 'react-router-dom';
import "./Task.scss";
import avatarPlaceHolder from '../../common/images/avatar-placeholder.gif'

function Task({ task }) {

  let {
    title, //String,
    location,// String,
    budget,// {price,unit:Enum['Hourly','Total']}
    dueDate,//date
    lastUpdatedTime,
    user,//{avatar,name}
    allowRemote,
    status, //enum[open, done, in progress],
    _id,
    category,
    userinfo,
  } = task;
  let posterAvatar = userinfo.avatar;
  //console.log("homepage task is used", document.location.pathname);
  return (

    <div className="my-task fade-effect">
      <Link to={"./tasks/id/" + _id} >

        <div className="my-task_category">{category}</div>
        <div className="my-task_body">
          <div className="my-task_avatar">

            <img src={posterAvatar} onError={(e) => { e.target.onerror = null; e.target.src = avatarPlaceHolder }} alt="avatar" />          </div>
          <div className="my-task_title">
            {title}
          </div>
          <div className="my-task_price">
            {budget}
          </div>
        </div>
        <div className="my-task_location">
          Location: {location}
        </div>
      </Link>

    </div>
  );
}

export default Task;
