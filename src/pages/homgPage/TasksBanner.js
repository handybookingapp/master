import React from 'react';
import Task from './Task';
import { isObjectEmpty } from '../../common/common';
class TasksBanner extends React.Component {


  componentDidMount() {

    if (isObjectEmpty(this.props.tasks)) {
      this.props.loadTasks();
    }
  }

  render() {
    if (isObjectEmpty(this.props.tasks)) {

      return <></>
    }
    // console.log(this.props.tasks)

    let tasksArray = Object.values(this.props.tasks);

    while (tasksArray.length > 0 && tasksArray.length < 20) {
      tasksArray.push(...tasksArray);
    }

    let halfwayThrough = Math.floor(tasksArray.length / 2)
    let tasks1stHalf = tasksArray.slice(0, halfwayThrough);
    let tasks2ndHalf = tasksArray.slice(halfwayThrough, tasksArray.length);

    return (
      <section className="recently-completed-tasks">
        <h1>We have been helping many people all along</h1>

        <div className="tasks-roll">

          <div className="roll roll1 ">
            {React.Children.toArray(tasks1stHalf.map((task) =>
              <Task task={task} />
            ))
            }
          </div>
          <div className="roll roll2 ">
            {React.Children.toArray(tasks2ndHalf.map((task) =>
              <Task task={task} />
            ))
            }
          </div>

        </div>

      </section>
    )
  }
}

export default TasksBanner;