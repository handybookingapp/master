import React from 'react';
import Worker from './../workerList/worklistcomponents/worker/Worker';

class TopWorkers extends React.Component {


  componentDidMount() {
    if (this.props.users.length === 0) {
      this.props.loadUsers();
    }

  }

  render() {
    let users = this.props.users;

    if (this.props.users.length === 0) {
      return <></>
    } else {

    }
    return (
      <section className="top-workers">
        {users.map((user) =>
          <Worker user={user} key={users.indexOf(user)} />
        )
        }
      </section>
    )
  }
}

export default TopWorkers;