import React from 'react';
import './worker-card.scss';

export default function Worker({ user }) {
  //console.log(user)
  return (
    // <Link to={`/profile/${user.id}`}>
    <div className="card_worklist" >
      <div className="card_worklist_up">
        <img src={user.avatar} alt="head" />
        <ul>
          <li className="name"><strong>{user.name}</strong></li>
          <li className="location">{user.location}</li>
          <li className="feedback">Commonds ({user.reviewnumber})</li>
        </ul>
      </div>
      <h3>LATEST REVIEW</h3>
      <p>{user.lastreview}</p>
      <h3>VERIFIED BADGES</h3>
      <img className="badges" title={user.phone} src="https://assets-airtasker-com.s3.amazonaws.com/uploads/badge/base_image/8/badge_poster_mobile-f564e506d0da034e60d8118e6eb5d387.png" alt="phone" />
      <a href="https://twitter.com/" target="_blank" rel="noopener noreferrer"><img className="badges" src="https://assets-airtasker-com.s3.amazonaws.com/uploads/badge/base_image/10/badge_poster_twitter-ade42d664c829000d1ffb227a7fa9e08.png" alt="twitter" /></a>
      <img className="badges" src="https://assets-airtasker-com.s3.amazonaws.com/uploads/badge/base_image/9/badge_poster_payment_method-9591a4a935b5e309dbef08dc00971096.png" alt="phone" />
    </div>
    // <Link/>
  );
}
