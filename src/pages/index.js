import { HomePage } from './homgPage'
import { Worker, WorkerList } from './workerList'
import { TaskList } from './taskList'
import PrivateUserProfile from './privateUserProfile'
import { UserprofilePublic } from './userprofilePublic'



export { HomePage, Worker, WorkerList, TaskList, PrivateUserProfile, UserprofilePublic };
export default { HomePage, Worker, WorkerList, TaskList, PrivateUserProfile, UserprofilePublic };