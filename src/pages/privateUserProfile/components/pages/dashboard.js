import React from 'react';
import ServicePicker from '../../../../common/components/ServicePicker/ServicePicker';
import './dashboard.scss';

function Dashboard() {
    return (
        <div className="private_main_dashboard">
            <div className="private_main_dashboard_header">
                <h4>password</h4>
            </div>
            <div className="private_main_dashboard_body">
                <h5>Get it done today</h5>
                <p>To-do list never getting shorter? Take the burden off and find the help you need on Airtasker.</p>
                <ServicePicker />
            </div>
        </div>
    )
}
export default Dashboard;