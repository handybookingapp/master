import React from 'react';
import { NavLink, Switch, Route, withRouter } from 'react-router-dom';


import './payment.scss';
import MakePayments from './paymentMake';
import ReceivePayments from './paymentReceive';

function PaymentMethods(props) {
  const url = props.location.pathname.replace("/privateUserProfile/payment/","")
  return (
    <div className="private_main_payment">
      <div className="private_main_payment_header">
        <h4>Payment methods</h4>
        <div>
          <NavLink to="/privateUserProfile/payment/" 
            className={url==="receive"?"":"private_main_payment_header--active"}
          > <h6>Make Payments</h6></NavLink>
          <NavLink to="/privateUserProfile/payment/receive"
            className={url==="receive"?"private_main_payment_header--active":""}
            ><h6>Receive Payments</h6></NavLink>
        </div>
      </div>
      <div className="private_main_payment_body">
        <Switch>
          <Route path='/privateUserProfile/payment/receive' exact component={ReceivePayments} />
          <Route path='/privateUserProfile/payment/' exact component={MakePayments} />
        </Switch>
      </div>
    </div>
  )
}

export default withRouter(PaymentMethods);

