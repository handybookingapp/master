import React from 'react';
import store from '../../../redux/store.js';
import './sidebar.scss';
import { NavLink } from "react-router-dom";



class Sidebar extends React.Component {
  constructor() {
    super();
    this.state = {
      showSettings: false,
    }
    this.toggleShowSettings = this.toggleShowSettings.bind(this)
  }
  toggleShowSettings() {
    this.setState({
      showSettings: this.state.showSettings ? false : true
    })
  }
  render() {
    let currentUser = store.getState().currentUser;

    console.log(currentUser);
    if (!currentUser) return <></>
    return (
      <div className="private_sidebar">
        <div>
          <img src={currentUser.avatar} alt="pending" />
          <div>{currentUser.firstName + ' ' + currentUser.lastName} </div>
        </div>
        <nav>
          <ul>
            <div className={this.state.showSettings ? "private_sidebar--settings" : "private_sidebar--default"}>
              <NavLink to="/privateUserProfile/dashboard"><li>Dashboard</li></NavLink>
              <NavLink to="/privateUserProfile/payment/"><li>Payment method</li></NavLink>
              <NavLink to="/privateUserProfile/paymentshistory"><li>Payments history</li></NavLink>
            </div>
            <li
              onClick={this.toggleShowSettings}
              className={this.state.showSettings ? "private_sidebar--settings" : "private_sidebar--default"}
            >
              settings<span></span></li>
            {/* The page only shows either default or settings at one time */}
            <div className={this.state.showSettings ? "private_sidebar--default" : "private_sidebar--settings"}>
              <NavLink to="/privateUserProfile/account"><li>account</li></NavLink>
              <NavLink to="/privateUserProfile/skills"><li>skills</li></NavLink>
              <NavLink to="/privateUserProfile/password"><li>password</li></NavLink>
            </div>
          </ul>
        </nav>
      </div>
    )
  }


}

export default Sidebar;
