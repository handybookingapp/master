import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import './style.scss';
import store from '../../redux/store.js';

import Sidebar from './components/sidebar';
import Account from './components/pages/account';
import Dashboard from './components/pages/dashboard';
import Password from './components/pages/password';
import Skills from './components/pages/skills';
import PaymentMethods from './components/pages/payment';
import paymentsHistory from './components/pages/paymentsHistory';
import { isObjectEmpty } from '../../common/common'


class PrivateUserProfile extends React.Component {
  componentDidMount() {
    const user = store.getState().currentUser;
    if (isObjectEmpty(user)) {
      document.location.href = '/';
    }

  }

  render() {
    return (
      <div className='private'>
        <Sidebar />
        <div className="private_main">
          <Switch>
            <Route path='/privateUserProfile/dashboard' component={Dashboard} />
            <Route path='/privateUserProfile/account' component={Account} />
            <Route path='/privateUserProfile/password' component={Password} />
            <Route path='/privateUserProfile/skills' component={Skills} />
            <Route path='/privateUserProfile/payment' component={PaymentMethods} />
            <Route path='/privateUserProfile/paymentshistory' component={paymentsHistory} />
          </Switch>
        </div>
      </div>
    )
  }

}
export default withRouter(PrivateUserProfile);
// const mapStateToProps = state => {
//     return {
//       focused: state.getIn(["header", "focused"]),
//       // focused: state.get('header').get('focused'),
//     };
//   };
//   const mapDispatchToProps = dispatch => {
//     return {
//       handleInputFocus(list) {
//         list.size === 0 && dispatch(actionCreators.getList());
//         //so only send the request once
//         dispatch(actionCreators.searchFocus());
//       },
//     };
//   };
//   export default connect(
//     mapStateToProps,
//     mapDispatchToProps
//   )(PrivateUserProfile);

