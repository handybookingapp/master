import React, { Component } from 'react';
import { postData } from '../../../Util';
import { taskCategories } from '../../../common/common';
import { NavLink } from 'react-router-dom';
import './searchBar.scss';
import { loadTasks } from '../../../redux/task/actions';
import { isObjectEmpty, getParamsFromURLNew } from '../../../common/common';




/*
逻辑 根据form选择不同会改变URL
根据URL会构建出不同filter给执行
*/


class SearchBar extends Component {

  constructor(props) {
    super(props);

    if (props.filter) {
      this.state = props.filter
    } else {
      this.state = {
        location: {
          remoteOnly: false,
          suburb: null
        },
        distance: null,
        price: null,
        category: taskCategories.Anything,
        availableOnly: false,
        searchText: ''
      }
    }

  }


  componentDidMount() {
    let url = window.location.pathname.replace('tasks/', '')

    // console.log(url);
    let { category, title } = getParamsFromURLNew(url);

    if (category) this.setState({ category })
    if (title) this.setState({ searchText: title })

    //for hightlighting the mytask button
    //   let a = url.match(/\/user\/[a-z0-9]*$/);
    //   if(a){

    //   }
    // }
  }


  baseURL = '/tasks'


  handleChangeCategory = event => {
    event.preventDefault();
    this.setState({ category: event.target.value })
  }
  handleChangeText = event => {
    event.preventDefault();
    this.setState({ searchText: event.target.value })
  }

  handleSearch = event => {//刷新页面，清空redux
    event.preventDefault();
    let searchLink = this.baseURL;
    searchLink += this.state.category ? '/category/' + this.state.category : '';
    searchLink += this.state.searchText ? '/title/' + this.state.searchText : '';
    window.location.href = searchLink;
  }
  /*
  search bar 结构：
  location
    remote only: boolean toggle
    suburb: input and dropdown
    distance: range bar
  price: range bar
  task type: 
    category: 
    available only: boolean toggle
  search text: input text
  */


  render() {
    const UserButtons = () => {
      if (!this.props.currentUser) return <></>
      let userID = this.props.currentUser.user;
      return (
        <div className="user-buttons">
          <NavLink className="my-tasks search-bar-button" to={this.baseURL + '/user/' + userID} onClick={() => this.props.loadTasksByUserID(userID)}>
            My Posted Tasks
        </NavLink>


          <NavLink className="my-tasks search-bar-button" to={this.baseURL + '/user/' + userID}>
            My Assigned Tasks
        </NavLink>
        </div>
      )
    }
    return (
      <div className="search-bar" >
        <div>
          <form onSubmit={this.handleSearch} className="search-bar_filters">

            <select className="search-bar-button " value={this.state.category} onChange={this.handleChangeCategory} searchable="true">
              {Object.values(taskCategories).map(
                (category) =>
                  <option value={category} key={category}>{category}</option>
              )}
            </select>

            <input type="text" className="search-text search-bar-button" value={this.state.searchText} placeholder="search in title" onChange={this.handleChangeText} >
            </input>

            <div onClick={this.handleSearch} className="search icon-search-magifyer"></div>

          </form>

          <UserButtons />
        </div>

      </div>

    )
  }

}
export default SearchBar;