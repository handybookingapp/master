import React from "react";
// import ReactDOM from "react-dom";
// import faker from "faker";
import "./stylesheet.scss";
import { Link } from 'react-router-dom';
import moment from 'moment';
import avatarPlaceHolder from '../../common/images/avatar-placeholder.gif'

function TaskBasicInfo({ task, currentTaskID }) {
  // //console.log(task)


  let {
    title, //String,
    location,// String,
    budget,// {price,unit:Enum['Hourly','Total']}
    dueDate,//date
    lastUpdatedTime,
    user,//{avatar,name}
    allowRemote,
    status, //enum[open, done, in progress],
    userinfo,
  } = task;
  let posterAvatar = userinfo.avatar;

  return (

    <Link to={'/tasks/id/' + task._id}>
      <div className={task._id === currentTaskID ? "task-basic current-task " : "task-basic "} >
        <div className="task-basic_leftpart">
          <h2 className="task-basic_leftpart_title">{title}</h2>
          {allowRemote && <p className="remote"> Remotely  </p>}
          <p> {location}</p>

          <p>{moment(dueDate).format("DD/MM/YYYY")}</p>
        </div>
        <div className="task-basic_rightpart">
          <p className="task-basic_rightpart_price">{budget}
          </p>
          <img src={posterAvatar} onError={(e) => { e.target.onerror = null; e.target.src = avatarPlaceHolder }} alt="avatar" />
        </div>
        <p className="task-basic_status">Status: {status}</p>
      </div>
    </Link>
  );
}

export default TaskBasicInfo;
