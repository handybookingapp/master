import React from "react";
// import ReactDOM from "react-dom";
import DialogTask from "./DialogTask"
import "./stylesheet.scss";
import moment from "moment";
import avatarPlaceHolder from '../../common/images/avatar-placeholder.gif'
import taskAPI from '../../services/taskAPI'
//若后期信息太多需要分两步fetch的话
//taskDetals 页面额外的信息are store as hashmap<key:'taskID',value:额外信息>
//in redux store
//componentdidmount的时候发检测到没有这个就 触发 load额外信息

//在不做questions 之前不需要做两步fetch。
class TaskDetailInfo extends React.Component {

  state = {
    isOpen: false,
    task: {},
    taskOptionsOpen: false
  }


  toggleTaskOptions = () => {
    this.setState({ taskOptionsOpen: (!this.state.taskOptionsOpen) });
  }

  editTask = () => {
    //跳转
    document.location.href = document.location.pathname + '/edit-task';
  }

  postSimilarTask = () => {
    document.location.href = document.location.pathname + '/post-similar-task';
  }

  cancelTask = () => {
    taskAPI.deleteTask(this.props.task).then((r) => {

      alert("Your task has been succesfully removed");
      document.location.pathname = 'tasks/user/' + this.props.currentUser.user;
    }
    );
    //跳出Pop up显示移除成功
    //
    //删除后刷新

  }


  render() {



    if (!this.props.task) return <></>;
    let {
      title,
      budget,
      location,
      detail,
      dueDate,
      allowRemote,
      lastUpdatedTime,
      category,
      status,
      userinfo
      // assignee
      // offer //*****需要offer crud支持
      // question //**需要question crud支持
    } = this.props.task
    let posterAvatar = userinfo.avatar;
    let posterName = userinfo.firstName + ' ' + userinfo.lastName;
    let statusArray = ['Open', 'Assigned', 'Completed'];
    let isMyTask = this.props.currentUser && this.props.currentUser.user == userinfo.user;
    return (
      <>
        <p className="status">
          {
            React.Children.toArray(statusArray.map(
              (s) => {
                if (s === status) {
                  return <span className="current-status">{s}</span>
                } else {
                  return <span>{s}</span>
                }
              }
            ))
          }
        </p>
        <div className="taskinfo">

          <div className="taskinfo_detail">

            <h2>{title}</h2>
            <p >POSTED BY:</p>
            <p className="taskinfo_detail_postername">{posterName}</p>
            <img src={posterAvatar} onError={(e) => { e.target.onerror = null; e.target.src = avatarPlaceHolder }} alt="avatar" />
            {allowRemote && <p className="remote taskinfo_detail_remote"> Remotely  </p>}

            <span>LOCATION  <a href="`https://www.google.com/maps/search/?api=1&amp;query=Bilgola+Plateau+NSW,+Australia`" target="_blank" rel="noopener noreferrer"><button className="taskinfo_detail_viewmap">View map</button></a></span>

            <p className="taskinfo_detail_location">{location}</p>

            <p>DUE DATE</p>
            <p >{moment(dueDate).format("DD/MM/YYYY")}</p>
            {/* <a href="`https://www.google.com/maps/search/?api=1&amp;query=Bilgola+Plateau+NSW,+Australia`" target="_blank" rel="noopener noreferrer"><button className="viewmap">View map</button></a> */}
          </div>
          <div className="taskinfo_taskpart">
            <div className="taskinfo_taskpart_taskbudget">
              <h3 className="taskinfo_taskpart_taskbudget_title">TASK BUDGET</h3>
              <p className="taskinfo_taskpart_taskbudget_cost"><strong>{budget}</strong></p>

              {!isMyTask &&
                <button className="taskinfo_taskpart_taskbudget_makeoffer" onClick={(e) => this.setState({ isOpen: true })}>Make an offer</button>
                ||
                <button className="taskinfo_taskpart_taskbudget_waitoffer" disabled>Awaiting offers</button>
              }

              {/* <select className="search-bar-button" value={this.state.category} onChange={this.handleChangeCategory} searchable="true">
                {Object.values(taskCategories).map(
                  (category) =>
                    <option value={category} key={category}>{category}</option>
                )}
              </select> */}



            </div>
            <div className="task-options icon-dropdown-arrow">
              <div onClick={this.toggleTaskOptions} className="task-options_title">
                More Options
               </div>

              {this.state.taskOptionsOpen &&
                <div className="task-options_dropdown">
                  {isMyTask && <div className="option" onClick={this.editTask} > Edit task </div>}
                  <div className="option" onClick={this.postSimilarTask}> Post a similar task  </div>
                  {isMyTask && <div className="option" onClick={this.cancelTask}> Cancel task </div>}
                </div>
              }
            </div>

            <div className="taskinfo_taskpart_share_box">
              <a href="https://twitter.com/" target="_blank" rel="noopener noreferrer"><img className="sharemedia" src="https://assets-airtasker-com.s3.amazonaws.com/uploads/badge/base_image/10/badge_poster_twitter-ade42d664c829000d1ffb227a7fa9e08.png" alt="twitter" /></a>
              <a href="https://facebook.com/" target="_blank" rel="noopener noreferrer"><img className="sharemedia" src="https://assets-airtasker-com.s3.amazonaws.com/uploads/badge/base_image/11/badge_poster_facebook-a64b2efb47f44bb8f84b78b83c5456f3.png" alt="facebook" /></a>
              <a href="https://linkedin.com/" target="_blank" rel="noopener noreferrer"><img className="sharemedia" src="https://assets-airtasker-com.s3.amazonaws.com/uploads/badge/base_image/12/badge_poster_linkedin-59a0b1e9ff6e3181a1e3393fe48d64b7.png" alt="linkinedin" /></a>
            </div>
          </div>
          <h3 className="detail">Details:</h3>
          <p>{detail}</p>
        </div>

        {this.props.currentUser
          &&
          <>
            <div className="offer">
              <h3 className="offer_title">offers</h3>
              {/* <img src="https://images/waiting-for-offers.png" alt="offer" /> */}
              {/* <button className="offer_button" onClick={(e) => this.setState({ isOpen: true })}>Make an offer</button> */}

              There is no offer for this task so far. Make an offer now and be the first one!
            </div>


            <div className="question">
              <h3 className="question_title">Questions (0)</h3>
              <p>Please don't share personal info – insurance won't apply to tasks not done through Airtasker!</p>

              {/* <img className="question_avatar" src={this.currentUser.avatar} alt="avatar" /> */}

              <form className="question_form">
                <input className="question_form_input" type="text" id="question" name="question" placeholder="Your question.." />
                <input className="question_form_submit" type="submit" value="Submit"></input>
              </form>
            </div>

            <DialogTask isOpen={this.state.isOpen} onClose={(e) => this.setState({ isOpen: false })}>
              <h2 className="offertitle1">Make an Offer</h2>
              <h3 className="offertitle2"> <strong>Your offer</strong></h3>
              <form className="form_offer">
                <input classNmae="form_input" type="text" id="offer_input" name="offer_input" placeholder={this.props.task.money} />
              </form>
              <ul className="offerul">
                <li className="offerli">BronzeService fee:</li>
                <li className="offerli" style={{ color: "rgb(85,90,117)" }}>-$33.00</li>
                <li className="offerli">You'll receive:</li>
                <li className="offerli" style={{ color: "rgb(85,90,117)" }}>{this.props.task.money - 33}</li>
              </ul>
              <h3 className="title">Why are you the best person for this task?</h3>
              <p className="offerdetail">For your safety, please do not share personal information, e.g., email, phone or address.</p>
              <form className="reason_form">
                <input className="reasonform_input" type="text" id="reasonoffer_input" name="reasonoffer_input" />
              </form>
              <button className="submitoffer">Submit Offer</button>

            </DialogTask>
          </>
        }

      </>

    );
  }
}

export default TaskDetailInfo;
