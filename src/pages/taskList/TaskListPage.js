import React from 'react';
import TaskBasicInfo from './TaskBasicInfo.js';
import TaskDetailInfo from './TaskDetailInfo.js';
import SearchBar from './SearchBar/SearchBar';
import PostTask from "../../common/components/PostTask/PostTask";
import { withRouter } from 'react-router-dom'
import { isObjectEmpty, getParamsFromURLNew } from '../../common/common';
import "./stylesheet.scss";

class TaskListPage extends React.Component {
  //   if (this.props.users[this.props.match.params.id]) {
  //     // //console.log(this.props.user);
  //     hasUser = true
  // 
  // constructor() {
  //   super();
  // }

  componentDidMount() {
    // window.scrollTo(0, 0);

    //TODO：reducer还没有针对返回数据为空数组的情况，可以在reducer接受到空数据是加入特定标记。
    // console.log("didmount")
    if (isObjectEmpty(this.props.tasks)) {//空数组，代表redux刚被清空
      if (this.props.match.params.user) {//针对/user/?user
        //console.log("user")
        this.props.loadTasksByUserID(this.props.match.params.user)
      }
      else { //根据路径做出执行不同的fetch tasks动作

        let url = window.location.pathname;
        if (url.match(/\/edit-task$/)) {
          url = url.replace('edit-task/', '');
        } else if (url.match(/\/post-similar-task$/)) {
          url = url.replace('post-similar-task/', '');
        }
        url = url.replace('tasks/', '');
        // console.log(url);
        let params = getParamsFromURLNew(url);

        if (!isObjectEmpty(params)) {//有param,针对task search造出来的url
          //console.log(params);
          if (params) {
            this.props.loadTasksByFilter(params);
          } else {
          }
        } else {//无param
          this.props.loadTasks();
        }
      }

    }

    //console.log(JSON.stringify(Object.values(this.props.tasks)[0]));


  }
  render() {
    // let hasTasker = false;
    // if (this.props.tasks[this.props.match.params.id]) {
    //   // //console.log(this.props.user);
    //   hasTasker = true
    // }
    //console.log(this.props.tasks)
    let currentTaskID = this.props.match.params.id;

    let task;
    if (currentTaskID) {
      //console.log(taskID)
      task = this.props.tasks[JSON.stringify(currentTaskID)];
    } else {
      task = Object.values(this.props.tasks)[0];
      if (task) {
        currentTaskID = task._id;
      }
    }
    let tasksArray = Object.values(this.props.tasks);
    //console.log(tasksArray);

    return (
      <div className="tasklist-page">
        <SearchBar
          loadTasks={this.props.loadTasks}
          loadTasksByFilter={this.props.loadTasksByFilter}
          loadTasksByUserID={this.props.loadTasksByUserID}
          currentUser={this.props.currentUser} />

        {
          (this.props.tasks.isEmpty !== true &&
            <div className="content">
              <div className="tasklist-page_side-bar">
                {React.Children.toArray(tasksArray.map((t) =>

                  <TaskBasicInfo task={t} currentTaskID={currentTaskID} />))}
              </div>

              <div className="tasklist-page_task-detail">
                {<TaskDetailInfo task={task} currentUser={this.props.currentUser} />}
              </div>
            </div >)
          ||
          <div className="message">
            Sorry, there is no task matches your search.
         </div >
        }
        {
          window.location.pathname.match(/\/edit-task$/)
          && task
          && <PostTask currentUser={this.props.currentUser} task={task} edit={true} />
        }
        {
          window.location.pathname.match(/\/post-similar-task$/)
          && task
          && <PostTask currentUser={this.props.currentUser} task={task} />
        }
      </div>

    );

  }
}
export default withRouter(TaskListPage);
