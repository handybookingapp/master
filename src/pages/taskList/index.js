
import TaskList from './TaskListPage';
import TaskBasicInfo from './TaskBasicInfo';
import TaskDetailInfo from './TaskDetailInfo';

export { TaskList, TaskBasicInfo, TaskDetailInfo };
export default { TaskList, TaskBasicInfo, TaskDetailInfo };
