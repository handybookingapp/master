import React from 'react';
import './userprofilePublic.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';
class UserDetial extends React.Component {


    constructor() {
        super();
        this.state = {
            hidden_tasker: { display: "" },
            hidden_poster: { display: "" },
            userData: '',
            review_array: [],
            category_array: []

        }
        this.hiddentasker = this.hiddentasker.bind(this)
        this.hiddenposter = this.hiddenposter.bind(this)
    }

    hiddentasker() {
        this.setState({
            hidden_poster: { display: "flex" },
            hidden_tasker: { display: "none" },
        })
    }

    hiddenposter() {
        this.setState({
            hidden_poster: { display: "none" },
            hidden_tasker: { display: "flex" }

        })
    }
    componentDidMount() {
        this.hiddenposter();
        if (this.props.user !== '') {
            this.setState({
                category_array: this.props.user.categories.map((category, index) => <li key={index}>{category}</li>),
                review_array: this.props.user.reviews.map((review, index) => <li key={index}>{review}</li>)
            })

        }
    }

    render() {
        // console.log(this.props)
        return (
            <>
                <div className="maincard_top">
                    <img src={this.props.user.avatar} alt="head" />
                </div>
                <div className="maincard_user-detail">
                    <ul>
                        <li ><strong>{this.props.user.firstName} {this.props.user.lastName} </strong></li>
                        <br />
                        {/* <li >Last online 2 hours ago</li> */}
                        <li><span>Location: </span> {this.props.user.location}</li>
                        <li><span>Start Time: </span> {String(this.props.user.createTime).substring(0, 10)}</li>
                    </ul>
                </div>
                <div className="maincard_payment">
                    <h1><strong>BADGES</strong></h1>
                    <p ><i>Payment Method</i></p>
                    <div className = "maincard_payment_img"><FontAwesomeIcon icon = {faCreditCard}/></div>
                    {/* <img src="https://assets-airtasker-com.s3.amazonaws.com/uploads/badge/base_image/9/badge_poster_payment_method-9591a4a935b5e309dbef08dc00971096.png" alt="patment" /> */}
                </div>

                <div className="maincard_reviews">
                    <h1 ><strong>Skills</strong></h1>
                    <p >{this.state.category_array}</p>
                    <h1 ><strong>REVIEWS</strong></h1>
                    <button onClick={this.hiddenposter}>As a Tasker</button> <button onClick={this.hiddentasker}>As a Poster</button>
                    <div style={this.state.hidden_poster}>
                        <p>{this.state.review_array}</p>
                    </div>

                    <div style={this.state.hidden_tasker}>
                        <p >This user has no reviews as a Tasker yet</p>
                    </div>
                </div>

            </>

        );

    }
}

export default UserDetial;

//直接用userapi.   直接在这个文件使用userapi.fetchUserinfo(basicid)
//或者使用redux，create field to store current public profile info in the store, implement reducer and action.