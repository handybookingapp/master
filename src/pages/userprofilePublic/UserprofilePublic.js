import React from 'react';
import UserDetial from './UserDetial'
import { withRouter } from 'react-router-dom'
import './userprofilePublic.scss'

class UserprofilePublic extends React.Component {
    constructor() {
        super();
        this.state = ({
            user: ''
        })
    }

    componentDidMount() {
        if (this.props.user === '' || this.props.user.user !== this.props.match.params.id) {
            this.props.loadUser(this.props.match.params.id);
        }
    }

    render() {
        // console.log(this.props)
        // console.log(this.props.match.params.id)
        return (
            <div className="maincard">
                {<UserDetial user={this.props.user} />}
            </div>
        );
    }
}

export default withRouter(UserprofilePublic);