import React from 'react';
import './workerList.scss';

export default function Review({ review }) {
    return (
        <div className="review_card">
            <div className="review_card_up">
                <img src={review.avatar} alt="head" />
                <p >{review.review}</p>
            </div>
            <p>{review.bookkeep} &nbsp; ${review.money}</p>
        </div>

    );
}