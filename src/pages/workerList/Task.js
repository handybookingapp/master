import React from 'react';
import './workerList.scss';

export default function Task({ task }) {
    return (

        <div className="recent_task_card">
            <h3>{task.task_name}</h3>
            <h4>${task.money}</h4>
            <span >{task.location} {task.date}</span>
            <p >{task.describe}</p>
        </div>
    );
}