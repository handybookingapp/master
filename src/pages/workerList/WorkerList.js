import React from 'react';
import './workerList.scss'
import { withRouter } from 'react-router-dom'
import Worker from './worklistcomponents/worker/Worker'
import { ServicePicker } from '../../common/components/ServicePicker/ServicePicker';
import Howitworks from './worklistcomponents/howitworks/Howitworks'
import Reasons from './worklistcomponents/reasons/Reason'

class WorkerList extends React.Component {
    componentDidMount() {

        // if (this.props.users.length === 0) {
        //     this.props.loadUsers();
        // }
    }

    render() {
        let category = this.props.match.params.cat;
        if (!category) category = "heroes"
        // console.log(this.props)
        return (
            <div className="worklist">
                <ServicePicker loadUsers={this.props.loadUsers} />

                <h2>Best rated {category}</h2>
                <div className="worklist_page">
                    {this.props.users.map(
                        (user) => <Worker user={user} key={this.props.users.indexOf(user)} />
                    )}
                </div>

                <h2 >What is Airtasker?</h2>
                <Howitworks />

                <h2 >Why find a {category} near you through Airtasker?</h2>
                <Reasons />
            </div>
        );
    }
}

export default withRouter(WorkerList);
