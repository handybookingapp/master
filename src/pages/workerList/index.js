import WorkerList from './WorkerList';
import Worker from './worklistcomponents/worker/Worker';

export { WorkerList, Worker };
export default { WorkerList, Worker };
