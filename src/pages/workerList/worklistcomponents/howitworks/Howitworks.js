import React from "react";
import '../../workerList.scss'

function Howitworks() {
    return (
        <div className="worklist_howitworks">
            <div className="worklist_howitworks_post-task">
                <img src="https://farm4.staticflickr.com/3658/3352780464_55444bffbb_b.jpg" alt="post task" />
                <p ><strong>Post your task</strong></p>
                <p className="worklist_howitworks_post-task_p">Tell us what you need, it's FREE to post.</p>
            </div>
            <div className="worklist_howitworks_post-task">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQcXdad4tdoVHRAUrTQJNUJbLizIAotRighMITHVKJNUSIAMAMG&s" alt="review offer" />
                <p ><strong>Review offers</strong></p>
                <p className="worklist_howitworks_posttask_p">Get offers from trusted Taskers and view profiles.</p>
            </div>
            <div className="worklist_howitworks_post-task">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTp-G5JzjPW5GQtFgnrSgDTziAtZATvyPLRT-EmqnHqXu7ZKgh0&s" alt="get done" />
                <p ><strong>Get it done</strong></p>
                <p className="worklist_howitworks_post-task_p">Choose the right person for your task and get it done.</p>
            </div>
            {/* <div className="worklist_howitworks_statis">
                <h1 className="worklist_howitworks_statis_num">531</h1>
                <p >Tasks successfully complete</p>
            </div>
            <div className="worklist_howitworks_statis">
                <h1 className="worklist_howitworks_statis_num">4</h1>
                <p >Avg. quotes per task</p>
            </div>
            <div className="worklist_howitworks_statis">
                <h1 className="worklist_howitworks_statis_num">4hrs</h1>
                <p >Avg. time to receive offer</p>
            </div> */}
        </div>
    )
}

export default Howitworks;