import React from 'react';
import '../../workerList.scss'

function Reasons(){
    return(
        <div className="worklist_why-keepper">
                    <div className="worklist_why-keepper_inside">
                    <img src="https://cdn.pixabay.com/photo/2019/10/02/20/59/clock-4522010_960_720.png" alt="clock" />
                        <h4>Quick offers</h4>
                        <p >Got a bit of a bookkeeping emergency? We’ll
                            help you connect with local bookkeepers near you who are available to help right away.
                        </p>
                    </div>
                    <div className="worklist_why-keepper_inside">
                    <img src="https://www.publicdomainpictures.net/pictures/270000/nahled/budget-for-new-ideas.jpg" alt="budget" />
                        <h4 >Choose your budget</h4>
                        <p >Get offers upfront and
                        find a bookkeeper near you who fits your budget and gets your business.
                        </p>
                    </div>
                    <div className="worklist_why-keepper_inside">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/0/0e/The_Best_FIFA_Football_Awards.jpg" alt="tasker" />
                        <h4 >Get the best taskers</h4>
                        <p >Getting the best bookkeeper
                        for the job is easy with our transparent ratings and reviews system.
                        </p>
                    </div>
                    <div className="worklist_why-keepper_inside">
                    <img src="https://live.staticflickr.com/3755/33756581805_b577e846fa_b.jpg" alt="insure" />
                        <h4 >Stay insured</h4>
                        <p >Hire with confidence
                        knowing that your bookkeeper is covered under our insurance policies.
                        </p>
                    </div>
                </div>
    )
}

export default Reasons;
