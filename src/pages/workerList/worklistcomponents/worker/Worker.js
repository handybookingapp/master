import React from 'react';
import { Link } from 'react-router-dom';
// import axios from "axios";
import './worker-card.scss';
import avatarPlaceHolder from '../../../../common/images/avatar-placeholder.gif'

class Worker extends React.Component {
  render() {
    // console.log(this.props.user.user)
    return (

      <div className="workcard" >
        <Link to={`/profile/${this.props.user.user}`}>
          <div className="workcard_up">
            <img src={this.props.user.avatar} alt="head" onError={(e) => { e.target.onerror = null; e.target.src = avatarPlaceHolder }} />

            <ul>
              <li ><strong>{this.props.user.lastName}</strong></li>
              <li className="workcard_up_location">{this.props.user.location}</li>
              <li >Reviews ({this.props.user.reviewsNumber})</li>
            </ul>
          </div>
        </Link>
        <h3>LATEST REVIEW</h3>
        <p>{this.props.user.reviews}</p>
        <h3>VERIFIED BADGES</h3>
        <img className="workcard_image" src="https://assets-airtasker-com.s3.amazonaws.com/uploads/badge/base_image/8/badge_poster_mobile-f564e506d0da034e60d8118e6eb5d387.png" alt="phone" />
        <div className="workcard_mobile">{this.props.user.mobileNumber}</div>
        <a href="https://twitter.com/" target="_blank" rel="noopener noreferrer"><img className="workcard_image" src="https://assets-airtasker-com.s3.amazonaws.com/uploads/badge/base_image/10/badge_poster_twitter-ade42d664c829000d1ffb227a7fa9e08.png" alt="twitter" /></a>
        <img className="workcard_image" src="https://assets-airtasker-com.s3.amazonaws.com/uploads/badge/base_image/9/badge_poster_payment_method-9591a4a935b5e309dbef08dc00971096.png" alt="phone" />
      </div>
    );
  }
}

export default Worker;
