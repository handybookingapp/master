import { applyMiddleware, createStore } from 'redux'//applyMiddleware is for use of thunk
import userReducers from './user/reducers'
import taskReducers from './task/reducers'

import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';
import { combineReducers } from 'redux';

// import location from './reducers'
let allReducers = combineReducers({
  ...userReducers,
  ...taskReducers
  //add more reducers here in the future.
})
export default createStore(allReducers, composeWithDevTools(applyMiddleware(thunk)));


