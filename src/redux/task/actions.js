import { LOAD_TASKS, LOAD_TASK, CREATE_TASK, UPDATE_TASK, DELETE_TASK } from './actionTypes';
// import backendAPI from '../../services/_backendAPI';
import taskAPI from '../../services/taskAPI';

//for home page
export const loadTasks = () => {

  return async dispatch => {

    taskAPI.fetchTasks().then(tasks => {
      dispatch({
        type: LOAD_TASKS,
        data: tasks
      });
    }
    );
  }
};

//for private profile
export const loadTasksByUserID = (userID) => {
  // console.log("action:loadTasks被调用了: filter:", userID)

  return async dispatch => {
    let tasks = await taskAPI.fetchTasksByUserID(userID)
    // console.log("action:loadTasks的二次dispatch会调用", tasks)
    // console.log(tasks);
    await dispatch({
      type: LOAD_TASKS,
      data: tasks
    });
  }
};

//for workerlistpage
/**
   * @description 
   * perform exact serch in category and  regex search with title. 
   * if no filter is provided then fatch all tasks
   * @param filter: { category, title }
   * example \{ category: 'Anything', title: '' }
   */
export const loadTasksByFilter = ({ category, title }) => {
  //console.log("action:loadTasks被调用了: filter:", filter)

  return async dispatch => {
    let tasks = await taskAPI.fetchTasksByFilter({ category, title })
    //console.log("action:loadTasks的二次dispatch会调用", tasks)
    //console.log(tasks);
    await dispatch({
      type: LOAD_TASKS,
      data: tasks
    });
  }
};



