import { LOAD_TASK, LOAD_TASKS, LOAD_MORE_TASKS } from './actionTypes';

/**
 * single task
 */
export const tasks = (oldState = {}, action) => {
  // console.log("reducer:tasks 被调用了", action)
  let state = {};

  switch (action.type) {

    //reloading tasks
    case (LOAD_TASKS):
      // console.log("reducer:tasks 匹配到 LOAD_TASKS")
      //console.log(action.data);

      if (action.data.length > 0) {

        action.data.map(task => {
          state[JSON.stringify(task._id)] = task
        }
        )
        return state;
      } else {
        return { isEmpty: true }
      }

    // add more tasks to oldstate
    case (LOAD_MORE_TASKS):
      state = oldState;
      if (action.data.length > 0) {
        action.data.map(task => {
          state[JSON.stringify(task._id)] = task
          // console.log(state);
        }
        )
      }
      return state

    default://初始化的时候也会用到
      // console.log("reducer tasks: 匹配到default", oldState)

      return oldState;
  }
}
export default {
  // task,
  tasks
}
