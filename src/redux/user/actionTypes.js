export const LOAD_USERS = 'loadUsers';
export const LOG_IN = 'login';
export const LOG_OUT = 'logout';
export const SIGN_UP = 'signUp';
export const INITIALIZE = 'Initialize';
export const LOAD_USER = 'loadUser';
