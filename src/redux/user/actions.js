import { LOAD_USERS, LOAD_USER, LOG_IN, SIGN_UP, LOG_OUT } from './actionTypes';
// import backendAPI from '../../services/_backendAPI';
import userAPI from '../../services/userAPI';

//for workerlistpage
export const loadUsers = (filter) => {
  // console.log("action:loadUsers被调用了: filter:", filter)
  return async dispatch => {
    let users = await userAPI.fetchUsersInfo(filter);
    // console.log("action:loadUsers的二次dispatch会调用", users)
    //console.log(users);
    await dispatch({
      type: LOAD_USERS,
      data: {
        users
      }
    });
  }
};


export const loadUser = (Id) => {
  console.log("action:loadUser被调用了: Id:", Id)
  return async dispatch => {
    let user = await userAPI.fetchUserInfo(Id);
    console.log("action:loadUser的二次dispatch会调用", user)
    await dispatch({
      type: LOAD_USER,
      data: {
        user
      }
    });
  }
};

//这里根据userid 去fetch user.
//现在这个已经是假设logged in.
export const logIn = (id) => {
  //console.log(id);
  return async dispatch => {
    //console.log("action:logIn的1次dispatch会调用")

    let user = await userAPI.fetchUserInfo(id);
    //console.log("action:logIn的二次dispatch会调用", user)
    //console.log(user);
    if (user === undefined) {
      user = null;
      //console.log("fetchUser user出了问题，是undefined");
    }
    await dispatch({
      type: LOG_IN,
      data: {
        user
      }
    });
  }
}



export const signUp = (id) => {
  return async dispatch => {
    let user = await userAPI.fetchUseri(id);
    // //console.log("action:loadUsers的二次dispatch会调用", users)
    // //console.log(users);
    await dispatch({
      type: SIGN_UP,
      data: {
        user
      }
    });
  }
}

export const logOut = () => {
  //console.log("action:loadUOut的被调用")
  return {
    type: LOG_OUT,
    data: null
  };
}



