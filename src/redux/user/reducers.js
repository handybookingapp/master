import { LOAD_USERS, LOAD_USER, LOG_IN, LOG_OUT, SIGN_UP } from './actionTypes';

export const users = (oldState = [], action) => {
  //console.log("reducer:users 被调用了")

  switch (action.type) {

    case (LOAD_USERS):
      //console.log("reducer:users 匹配到 LOAD_USERS")

      return action.data.users;

    default://初始化的时候也会用到
      //console.log("reducer users:匹配到default", oldState)

      return oldState;
  }
}
export const user = (oldState = '', action) => {
  // console.log("reducer:users 被调用了")

  switch (action.type) {

    case (LOAD_USER):
      // console.log("reducer:users 匹配到 LOAD_USERS")
      // console.log(action.data.user)

      return action.data.user;

    default://初始化的时候也会用到
      // console.log("reducer users:匹配到default", oldState)

      return oldState;
  }
}
//TODO: user empty for default
export const currentUser = (oldState = null, action) => {
  // //console.log("currentUsr 接收到的actiontype是" + action.type)

  switch (action.type) {
    case (LOG_IN):
      //console.log("action.data是" + JSON.stringify(action.data))
      return action.data.user;

    case (LOG_OUT):
      //console.log("action.data是" + JSON.stringify(action.data))
      window.localStorage.removeItem('token')
      return null;

    case (SIGN_UP):
      return action.data.user;

    default://初始化的时候也会用到
      //console.log("reducer currentUser:匹配到default", oldState)

      return oldState;
  }
}


export default {
  users,
  user,
  currentUser
}
