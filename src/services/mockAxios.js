import MockAdapter from 'axios-mock-adapter';
import { taskCategories } from '../common/common';
import axios from 'axios';
import faker from 'faker';
//=========faking data=======
// This sets the mock adapter on the default instance
// export function pauseMock() {
//   mock.restore();
// }
// Mock any GET request to /backendurl/users
// arguments for reply are (status, data, headers)
// mock.onGet('backendurl/users/').reply(200, {
//   users: makeUsers(10)
// });
//=====================
function makeUsers(n) {
  let users = [];
  for (let i = 0; i < n; i++) {
    let user =
    {
      categories: [faker.random.objectElement(taskCategories)],
      name: faker.name.firstName(),
      location: faker.address.city(),
      reviewnumber: faker.random.number(10),
      lastreview: faker.lorem.sentences(4),
      avatar: faker.image.avatar(),
      phone: faker.phone.phoneNumber()
    }
    // //console.log(user.categories)
    users.push(user);
  }
  // console.log(users)
  return users;

}


function test(n) {
  let users = [];
  // let name = "",location = "",reviewnumber="",lastreview="",phone="",categories=""
  const url = 'http://127.0.0.1:8080/userinfo/';
  axios.get(url).then(
    res => {
      for (let i = 0; i < n; i++) {
        let user = {
          categories: [faker.random.objectElement(taskCategories)],
          name: res.data[i].firstName,
          location: res.data[i].location,
          reviewnumber: res.data[i].commondsNumber,
          lastreview: res.data[i].commonds,
          phone: res.data[i].mobileNumber
        }
        users.push(user)
      }
    }
  )
  // console.log(users)
  return users

}
// console.log(test(10))

function makeUserWithCat(c, n) {
  // let users = makeUsers(n);
  // console.log(test(10))
  let users = test(n)

  users.map((user) => { user.categories = [c] })
  return users;
}

function makeUserMocker() {
  const mock = new MockAdapter(axios);

  mock.onGet(`backendurl/users/${encodeURI(taskCategories.Business)}`).reply(200, {
    users: makeUserWithCat(taskCategories.Business, 10)
  });
  mock.onGet(`backendurl/users/${encodeURI(taskCategories.Anything)}`).reply(200, {
    users: makeUserWithCat(taskCategories.Anything, 10)
  });
  mock.onGet(`backendurl/users/${encodeURI(taskCategories.Cleaning)}`).reply(200, {
    users: makeUserWithCat(taskCategories.Cleaning, 10)
  });
  mock.onGet(`backendurl/users/${encodeURI(taskCategories.Delivery)}`).reply(200, {
    users: makeUserWithCat(taskCategories.Delivery, 10)
  });
  mock.onGet(`backendurl/users/${encodeURI(taskCategories.Furniture)}`).reply(200, {
    users: makeUserWithCat(taskCategories.Furniture, 10)
  });
  mock.onGet(`backendurl/users/${encodeURI(taskCategories.Handyman)}`).reply(200, {
    users: makeUserWithCat(taskCategories.Handyman, 10)
  });
  mock.onGet(`backendurl/users/${encodeURI(taskCategories.Home)}`).reply(200, {
    users: makeUserWithCat(taskCategories.Home, 10)
  });
  mock.onGet(`backendurl/users/${encodeURI(taskCategories.Marketing)}`).reply(200, {
    users: makeUserWithCat(taskCategories.Marketing, 10)
  });
  mock.onGet('backendurl/users').reply(200, {
    users: makeUsers(10)

  });
  return mock;

}

export default { makeUserMocker };
export { makeUserMocker };